package com.example.sampleapp.presentation;

import android.support.annotation.NonNull;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;

import java.util.Collections;
import java.util.List;

import com.example.sampleapp.base.BaseRxTest;
import com.example.sampleapp.domain.GetPostInteractor;
import com.example.sampleapp.domain.models.PostWithUserAvatar;
import com.example.sampleapp.presentation.model.PostModelMapper;
import com.example.sampleapp.presentation.post.mvp.PostPresenter;
import com.example.sampleapp.presentation.post.mvp.PostView;
import io.reactivex.Flowable;
import io.reactivex.processors.BehaviorProcessor;
import polanski.option.Option;

public class PostPresenterTest extends BaseRxTest {

    @Mock GetPostInteractor interactor;
    @Mock PostView postView;

    private PostPresenter presenter;
    private StreamBuilder streamBuilder;

    @Before
    public void before() {
        presenter = new PostPresenter(interactor, new PostModelMapper());
        presenter.attachView(postView);
        streamBuilder = new StreamBuilder();
    }

    @Test
    public void loadPostsSuccessFully() {
        final List<PostWithUserAvatar> list = createList();
        streamBuilder.emitPosts(interactor, list);

        presenter.loadPosts();

        Mockito.verify(postView).showLoadingBar();
        Mockito.verify(postView).hideLoadingBar();
        Mockito.verify(postView).loadPostList(Mockito.anyList());
        Mockito.verify(postView, Mockito.never()).showError(Mockito.any());
    }

    @Test
    public void whenLoadPostsErrorOccur() {
        final Throwable error = Mockito.mock(Throwable.class);
        streamBuilder.emitError(interactor, error);

        presenter.loadPosts();

        Mockito.verify(postView).showLoadingBar();
        Mockito.verify(postView).hideLoadingBar();
        Mockito.verify(postView, Mockito.never()).loadPostList(Mockito.anyList());
        Mockito.verify(postView).showError(error);
    }

    @NonNull
    private List<PostWithUserAvatar> createList() {
        return Collections.emptyList();
    }

    private static class StreamBuilder {
        private final BehaviorProcessor<List<PostWithUserAvatar>> stream = BehaviorProcessor.create();

        private void emitPosts(@NonNull GetPostInteractor interactor,
                               @NonNull List<PostWithUserAvatar> posts) {
            Mockito.when(interactor.getResult(Option.none())).thenReturn(stream);
            stream.onNext(posts);
        }

        private void emitError(@NonNull GetPostInteractor interactor,
                               @NonNull Throwable error) {
            Mockito.when(interactor.getResult(Option.none())).thenReturn(Flowable.error(error));
        }
    }
}
