package com.example.sampleapp.presentation;

import android.support.annotation.NonNull;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;

import com.example.sampleapp.base.BaseRxTest;
import com.example.sampleapp.dataProvider.DummyPostDetails;
import com.example.sampleapp.domain.GetPostDetailsInteractor;
import com.example.sampleapp.domain.models.PostDetails;
import com.example.sampleapp.presentation.model.PostDetailsMapper;
import com.example.sampleapp.presentation.model.PostModelMapper;
import com.example.sampleapp.presentation.model.UserModelMapper;
import com.example.sampleapp.presentation.postDetails.mvp.PostDetailsPresenter;
import com.example.sampleapp.presentation.postDetails.mvp.PostDetailsView;
import io.reactivex.Flowable;
import io.reactivex.processors.BehaviorProcessor;
import polanski.option.Option;

public class PostDetailsPresenterTest extends BaseRxTest {

    @Mock GetPostDetailsInteractor interactor;
    @Mock PostDetailsView postDetailsView;

    private PostDetailsPresenter presenter;
    private StreamBuilder streamBuilder;

    private static final int RANDOM_ID = 1;

    @Before
    public void before() {
        final UserModelMapper userModelMapper = new UserModelMapper();
        final PostModelMapper postModelMapper = new PostModelMapper();
        final PostDetailsMapper postDetailsMapper = new PostDetailsMapper(userModelMapper, postModelMapper);
        presenter = new PostDetailsPresenter(interactor, postDetailsMapper);
        presenter.attachView(postDetailsView);
        streamBuilder = new StreamBuilder();
    }

    @Test
    public void loadPostsSuccessFully() {
        streamBuilder.emitPosts(interactor, RANDOM_ID);
        presenter.loadPostDetails(RANDOM_ID);

        Mockito.verify(postDetailsView).showLoadingBar();
        Mockito.verify(postDetailsView).hideLoadingBar();
        Mockito.verify(postDetailsView).loadPostDetails(Mockito.any());
        Mockito.verify(postDetailsView, Mockito.never()).showError(Mockito.any());
    }

    @Test
    public void whenLoadPostsErrorOccur() {
        final Throwable error = Mockito.mock(Throwable.class);
        streamBuilder.emitError(interactor, error, RANDOM_ID);
        presenter.loadPostDetails(RANDOM_ID);

        Mockito.verify(postDetailsView).showLoadingBar();
        Mockito.verify(postDetailsView).hideLoadingBar();
        Mockito.verify(postDetailsView, Mockito.never()).loadPostDetails(Mockito.any());
        Mockito.verify(postDetailsView).showError(error);
    }

    private static class StreamBuilder {
        private final BehaviorProcessor<PostDetails> stream = BehaviorProcessor.create();

        private void emitPosts(@NonNull GetPostDetailsInteractor interactor, int randomInt) {
            Mockito.when(interactor.getResult(Option.ofObj(randomInt))).thenReturn(stream);
            stream.onNext(DummyPostDetails.create(RANDOM_ID));
        }

        private void emitError(@NonNull GetPostDetailsInteractor interactor,
                               @NonNull Throwable error,
                               int randomInt) {
            Mockito.when(interactor.getResult(Option.ofObj(randomInt))).thenReturn(Flowable.error(error));
        }
    }
}
