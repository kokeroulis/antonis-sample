package com.example.sampleapp.helpers;

import android.support.annotation.NonNull;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import com.example.sampleapp.base.UnitTestApplication;
import com.example.sampleapp.data.network.ReplaceBaseUrl;
import okhttp3.mockwebserver.MockResponse;
import okhttp3.mockwebserver.MockWebServer;

public class MockServerSetupHelper {

    @NonNull
    public MockWebServer createAndSetupMockWebServer() throws IOException {
        final MockWebServer mockWebServer = new MockWebServer();
        mockWebServer.start();

        final ReplaceBaseUrl baseUrl = UnitTestApplication.getApplicationComponent().replaceBaseUrl();
        baseUrl.changeUrl(mockWebServer.url("").toString());
        return mockWebServer;
    }

    public void enqueueJsonAssetToMockWebServer(@NonNull String assetName,
                                                @NonNull MockWebServer mockWebServer) throws IOException {
        final String json = readStream(assetToStream(assetName));
        mockWebServer.enqueue(new MockResponse().setBody(json));
    }

    private InputStream assetToStream(@NonNull String assetName) throws IOException {
         return getClass().getResourceAsStream(assetName);
    }

    private String readStream(InputStream iStream) throws IOException {

        //Buffered reader allows us to read line by line
        try (BufferedReader bReader =
                 new BufferedReader(new InputStreamReader(iStream))){
            StringBuilder builder = new StringBuilder();
            String line;
            while((line = bReader.readLine()) != null) {  //Read till end
                builder.append(line);
                builder.append("\n"); // append new line to preserve lines
            }
            return builder.toString();
        }
    }
}
