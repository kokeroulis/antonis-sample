package com.example.sampleapp.base;

import com.example.sampleapp.MyApplication;
import com.example.sampleapp.dagger.components.ApplicationComponent;

public class UnitTestApplication extends MyApplication {

    public static ApplicationComponent getApplicationComponent() {
        return applicationComponent;
    }

}
