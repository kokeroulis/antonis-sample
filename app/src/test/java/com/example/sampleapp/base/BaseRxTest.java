package com.example.sampleapp.base;

import org.junit.Rule;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

@RunWith(CustomRobolectricTestRunner.class)
public abstract class BaseRxTest {

    @Rule
    public final MockitoRule rule = MockitoJUnit.rule();

    @Rule
    public final RxOverrideScheduler rxOverrideScheduler = new RxOverrideScheduler();
}
