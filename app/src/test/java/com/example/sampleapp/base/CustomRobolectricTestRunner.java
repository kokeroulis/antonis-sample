package com.example.sampleapp.base;

import org.junit.runners.model.InitializationError;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;

import com.example.sampleapp.BuildConfig;

public class CustomRobolectricTestRunner extends RobolectricTestRunner {

    private static final int SDK_API_LEVEL_TO_EMULATE = 23;

    public CustomRobolectricTestRunner(Class<?> klass) throws InitializationError {
        super(klass);
    }

    @Override
    protected Config buildGlobalConfig() {
        return new Config.Builder()
            .setSdk(SDK_API_LEVEL_TO_EMULATE)
            .setConstants(BuildConfig.class)
            .build();
    }
}
