package com.example.sampleapp.domain;

import android.support.annotation.NonNull;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;

import java.util.Collections;
import java.util.List;

import com.example.sampleapp.base.BaseRxTest;
import com.example.sampleapp.dataProvider.DummyComment;
import com.example.sampleapp.dataProvider.DummyPost;
import com.example.sampleapp.dataProvider.DummyUser;
import com.example.sampleapp.domain.models.Comment;
import com.example.sampleapp.domain.models.Post;
import com.example.sampleapp.domain.models.PostDetails;
import com.example.sampleapp.domain.models.User;
import io.reactivex.Completable;
import io.reactivex.processors.BehaviorProcessor;
import io.reactivex.subscribers.TestSubscriber;
import polanski.option.Option;

public class GetPostDetailsInteractorTest extends BaseRxTest {

    private GetPostDetailsInteractor interactor;

    @Mock PostRepository postRepository;
    @Mock UserRepository userRepository;
    @Mock CommentRepository commentRepository;
    private StreamBuilder streamBuilder;
    private Post post;
    private User user;

    public static final int RANDOM_ID = 1;

    @Before
    public void before() {
        interactor = new GetPostDetailsInteractor(postRepository, userRepository, commentRepository);
        streamBuilder = new StreamBuilder();
        post= DummyPost.create().get(0);
        user = DummyUser.create().get(0);
    }

    @Test
    public void dontForceUpdateWhenCommentsExist() {
        final List<Comment> commentList = DummyComment.create();
        streamBuilder.populateUserAndPost(postRepository, userRepository, post, user)
            .emitListFromRepo(commentRepository, commentList);
        final TestSubscriber<PostDetails> testSubscriber = interactor
            .getResult(Option.ofObj(RANDOM_ID)).test();

        testSubscriber.assertNoErrors();
        testSubscriber.assertNotTerminated();

        Mockito.verify(userRepository, Mockito.never()).forceUpdate();
        Mockito.verify(postRepository, Mockito.never()).forceUpdate();
    }

    @Test
    public void fetchCommentsAndRetrieve() {
        final List<Comment> commentList = DummyComment.create();

        streamBuilder.populateUserAndPost(postRepository, userRepository, post, user)
            .emitListFromCloudAndThenToRepo(commentRepository, commentList);
        final TestSubscriber<PostDetails> testSubscriber = interactor
            .getResult(Option.ofObj(RANDOM_ID)).test();

        testSubscriber.assertValueCount(1);
        testSubscriber.assertNoErrors();
        testSubscriber.assertNotTerminated();

        Mockito.verify(commentRepository).forceUpdate();
    }

    @Test
    public void fetchPostAndUsersAndFailOnNetwork() {
        final Throwable error = Mockito.mock(Throwable.class);

        streamBuilder
            .populateUserAndPost(postRepository, userRepository, post, user)
            .emitErrorFromCloudWhenFetching(commentRepository, error);

        final TestSubscriber<PostDetails> testSubscriber = interactor
            .getResult(Option.ofObj(RANDOM_ID)).test();

        testSubscriber.assertError(error);
        testSubscriber.assertTerminated();
    }

    private static class StreamBuilder {
        private final BehaviorProcessor<Post> postStream = BehaviorProcessor.create();
        private final BehaviorProcessor<Option<User>> userStream = BehaviorProcessor.create();
        private final BehaviorProcessor<List<Comment>> commentStream = BehaviorProcessor.create();


        private StreamBuilder populateUserAndPost(@NonNull PostRepository postRepository,
                                         @NonNull UserRepository userRepository,
                                         @NonNull Post post, @NonNull User user) {
            Mockito.when(postRepository.findPostById(Mockito.anyInt())).thenReturn(postStream);
            postStream.onNext(post);

            Mockito.when(userRepository.getUserById(Mockito.anyInt())).thenReturn(userStream);
            userStream.onNext(Option.ofObj(user));
            return this;
        }

        private void emitListFromRepo(@NonNull CommentRepository commentRepository,
                                      @NonNull List<Comment> commentList) {
            Mockito.when(commentRepository.getCommentByPostId(Mockito.anyInt())).thenReturn(commentStream);
            commentStream.onNext(commentList);
        }

        private void emitListFromCloudAndThenToRepo(@NonNull CommentRepository commentRepository,
                                                    @NonNull List<Comment> commentList) {
            // empty an empty list to show the absence of data
            commentStream.onNext(Collections.emptyList());
            Mockito.when(commentRepository.getCommentByPostId(Mockito.anyInt())).thenReturn(commentStream);

            Mockito.when(commentRepository.getCommentByPostId(Mockito.anyInt())).thenReturn(commentStream);
            Mockito.doAnswer(invocation -> {
                commentStream.onNext(commentList);
                return Completable.complete();
            }).when(commentRepository).forceUpdate();
        }

        private void emitErrorFromCloudWhenFetching(@NonNull CommentRepository commentRepository,
                                                    @NonNull Throwable error) {

            Mockito.when(commentRepository.getCommentByPostId(Mockito.anyInt())).thenReturn(commentStream);
            // empty an empty list to show the absence of data
            commentStream.onNext(Collections.emptyList());
            Mockito.when(commentRepository.forceUpdate()).thenReturn(Completable.error(error));
        }
    }
}
