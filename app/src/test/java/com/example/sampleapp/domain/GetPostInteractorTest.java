package com.example.sampleapp.domain;

import android.support.annotation.NonNull;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;

import java.util.Collections;
import java.util.List;

import com.example.sampleapp.base.BaseRxTest;
import com.example.sampleapp.dataProvider.DummyPost;
import com.example.sampleapp.dataProvider.DummyUser;
import com.example.sampleapp.domain.models.Post;
import com.example.sampleapp.domain.models.PostWithUserAvatar;
import com.example.sampleapp.domain.models.User;
import io.reactivex.Completable;
import io.reactivex.processors.BehaviorProcessor;
import io.reactivex.subscribers.TestSubscriber;
import polanski.option.Option;

public class GetPostInteractorTest extends BaseRxTest {


    private GetPostInteractor interactor;

    @Mock PostRepository postRepository;
    @Mock UserRepository userRepository;
    private StreamBuilder streamBuilder;

    @Before
    public void before() {
        interactor = new GetPostInteractor(postRepository, userRepository);
        streamBuilder = new StreamBuilder();
    }

    @Test
    public void dontForceUpdateWhenPostsAndUsersExist() {
        final List<Post> postList = DummyPost.create();
        final List<User> userList = DummyUser.create();

        streamBuilder.emitListFromRepo(postRepository, userRepository, postList, userList);
        final TestSubscriber<List<PostWithUserAvatar>> testSubscriber = interactor.getResult(Option.none()).test();

        testSubscriber.assertNoErrors();
        testSubscriber.assertNotTerminated();

        Mockito.verify(userRepository, Mockito.never()).forceUpdate();
        Mockito.verify(postRepository, Mockito.never()).forceUpdate();
    }

    @Test
    public void fetchPostsAndUsersAndRetrieve() {
        final List<Post> postList = DummyPost.create();
        final List<User> userList = DummyUser.create();

        streamBuilder.emitListFromCloudAndThenToRepo(postRepository, userRepository, postList, userList);
        final TestSubscriber<List<PostWithUserAvatar>> testSubscriber = interactor.getResult(Option.none()).test();

        testSubscriber.assertValueCount(1);
        testSubscriber.assertNoErrors();
        testSubscriber.assertNotTerminated();

        Mockito.verify(userRepository).forceUpdate();
        Mockito.verify(postRepository).forceUpdate();
    }

    @Test
    public void fetchPostAndUsersAndFailOnNetwork() {
        final List<Post> postList = DummyPost.create();
        final List<User> userList = DummyUser.create();
        final Throwable error = Mockito.mock(Throwable.class);
        streamBuilder.emitErrorFromCloudWhenFetching(postRepository, userRepository, error);
        final TestSubscriber<List<PostWithUserAvatar>> testSubscriber = interactor.getResult(Option.none()).test();

        testSubscriber.assertError(error);
        testSubscriber.assertTerminated();
    }

    private static class StreamBuilder {
        private final BehaviorProcessor<List<Post>> postStream = BehaviorProcessor.create();
        private final BehaviorProcessor<List<User>> userStream = BehaviorProcessor.create();

        private void emitListFromRepo(@NonNull PostRepository postRepository, @NonNull UserRepository userRepository,
                                      @NonNull List<Post> postsList, @NonNull List<User> userList) {
            Mockito.when(postRepository.getPosts()).thenReturn(postStream);
            postStream.onNext(postsList);

            Mockito.when(userRepository.getUsers()).thenReturn(userStream);
            userStream.onNext(userList);
        }

        private void emitListFromCloudAndThenToRepo(@NonNull PostRepository postRepository,
                                                    @NonNull UserRepository userRepository,
                                                    @NonNull List<Post> postsList,
                                                    @NonNull List<User> userList) {
            // empty an empty list to show the absence of data
            postStream.onNext(Collections.emptyList());
            Mockito.when(postRepository.getPosts()).thenReturn(postStream);

            // empty an empty list to show the absence of data
            userStream.onNext(Collections.emptyList());
            Mockito.when(userRepository.getUsers()).thenReturn(userStream);

            Mockito.doAnswer(invocation -> {
                userStream.onNext(userList);
                return Completable.complete();
            }).when(userRepository).forceUpdate();

            Mockito.doAnswer(invocation -> {
                postStream.onNext(postsList);
                return Completable.complete();
            }).when(postRepository).forceUpdate();
        }

        private void emitErrorFromCloudWhenFetching(@NonNull PostRepository postRepository,
                                                    @NonNull UserRepository userRepository,
                                                    @NonNull Throwable error) {
            // empty an empty list to show the absence of data
            postStream.onNext(Collections.emptyList());
            Mockito.when(postRepository.getPosts()).thenReturn(postStream);

            // empty an empty list to show the absence of data
            userStream.onNext(Collections.emptyList());
            Mockito.when(userRepository.getUsers()).thenReturn(userStream);

            Mockito.when(postRepository.forceUpdate()).thenReturn(Completable.error(error));
            Mockito.when(userRepository.forceUpdate()).thenReturn(Completable.error(error));
        }
    }
}
