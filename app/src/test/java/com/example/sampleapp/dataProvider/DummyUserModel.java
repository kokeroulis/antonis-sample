package com.example.sampleapp.dataProvider;

import android.support.annotation.NonNull;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import com.example.sampleapp.presentation.model.UserModel;

public class DummyUserModel {

    @NonNull
    public static List<UserModel> create() {
        final UserModel first = UserModel.builder()
            .id(1)
            .email("Sincere@april.biz")
            .userName("Bret")
            .build();

        final UserModel second = UserModel.builder()
            .id(2)
            .email("Shanna@melissa.tv")
            .userName("Antonette")
            .build();

        return Collections.unmodifiableList(Arrays.asList(first, second));
    }
}
