package com.example.sampleapp.dataProvider;

import android.support.annotation.NonNull;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import com.example.sampleapp.data.network.CommentResponse;

public class DummyCommentsResponse {

    @NonNull
    public static List<CommentResponse> create() {
        final CommentResponse first = CommentResponse.builder()
            .id(1)
            .postId(1)
            .email("Eliseo@gardner.biz")
            .name("id labore ex et quam laborum")
            .body("some body 111")
            .build();

        final CommentResponse second = CommentResponse.builder()
            .id(2)
            .postId(2)
            .email("Jayne_Kuhic@sydney.com")
            .name("quo vero reiciendis velit similique earum")
            .body("some body 222")
            .build();

        return Collections.unmodifiableList(Arrays.asList(first, second));
    }
}
