package com.example.sampleapp.dataProvider;

import android.support.annotation.NonNull;

import com.example.sampleapp.domain.models.PostWithUserAvatar;
import com.example.sampleapp.presentation.model.PostDetailsModel;
import com.example.sampleapp.presentation.model.PostModel;
import com.example.sampleapp.presentation.model.UserModel;

public class DummyPostDetailsModel {

    @NonNull
    public static PostDetailsModel create(int commentCount) {

        final UserModel user = UserModel.builder()
            .id(1)
            .email("Sincere@april.biz")
            .userName("Bret")
            .build();

        final PostModel post = PostModel.builder()
            .id(1)
            .userId(1)
            .avatarUrl(PostWithUserAvatar.createUserAvatar(user.email()))
            .title("sunt aut facere repellat provident occaecati excepturi optio reprehenderit")
            .body("quia et suscipit suscipit recusandae consequuntur expedita et cum reprehenderit molestiae ut ut quas totam nostrum rerum est autem sunt rem eveniet architecto")
            .build();

        return PostDetailsModel.builder()
            .postModel(post)
            .userModel(user)
            .commentCount(commentCount)
            .build();
    }
}
