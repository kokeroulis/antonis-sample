package com.example.sampleapp.dataProvider;

import android.support.annotation.NonNull;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import com.example.sampleapp.domain.models.User;

public class DummyUser {

    @NonNull
    public static List<User> create() {
        final User first = User.builder()
            .id(1)
            .email("Sincere@april.biz")
            .userName("Bret")
            .build();

        final User second = User.builder()
            .id(2)
            .email("Shanna@melissa.tv")
            .userName("Antonette")
            .build();

        return Collections.unmodifiableList(Arrays.asList(first, second));
    }
}
