package com.example.sampleapp.dataProvider;

import android.support.annotation.NonNull;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import com.example.sampleapp.domain.models.PostWithUserAvatar;
import com.example.sampleapp.presentation.model.PostModel;

public class DummyPostModel {

    @NonNull
    public static List<PostModel> create(@NonNull String email) {
        final PostModel first = PostModel.builder()
            .id(1)
            .userId(1)
            .avatarUrl(PostWithUserAvatar.createUserAvatar(email))
            .title("sunt aut facere repellat provident occaecati excepturi optio reprehenderit")
            .body("quia et suscipit suscipit recusandae consequuntur expedita et cum reprehenderit molestiae ut ut quas totam nostrum rerum est autem sunt rem eveniet architecto")
            .build();

        final PostModel second = PostModel.builder()
            .id(2)
            .userId(2)
            .avatarUrl(PostWithUserAvatar.createUserAvatar(email))
            .title("qui est esse")
            .body("est rerum tempore vitae sequi sint nihil reprehenderit dolor beatae ea dolores neque fugiat blanditiis voluptate porro vel nihil molestiae ut reiciendis qui aperiam non debitis possimus qui neque nisi nulla")
            .build();

        return Collections.unmodifiableList(Arrays.asList(first, second));
    }
}
