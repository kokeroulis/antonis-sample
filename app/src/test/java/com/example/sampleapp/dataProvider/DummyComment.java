package com.example.sampleapp.dataProvider;

import android.support.annotation.NonNull;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import com.example.sampleapp.domain.models.Comment;

public class DummyComment {

    @NonNull
    public static List<Comment> create() {
        final Comment first = Comment.builder()
            .id(1)
            .postId(1)
            .build();

        final Comment second = Comment.builder()
            .id(2)
            .postId(2)
            .build();

        return Collections.unmodifiableList(Arrays.asList(first, second));
    }
}
