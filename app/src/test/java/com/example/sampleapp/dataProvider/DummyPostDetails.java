package com.example.sampleapp.dataProvider;

import android.support.annotation.NonNull;

import com.example.sampleapp.domain.models.Post;
import com.example.sampleapp.domain.models.PostDetails;
import com.example.sampleapp.domain.models.User;

public class DummyPostDetails {

    @NonNull
    public static PostDetails create(int commentCount) {
        final Post post = Post.builder()
            .id(1)
            .userId(1)
            .title("sunt aut facere repellat provident occaecati excepturi optio reprehenderit")
            .body("quia et suscipit suscipit recusandae consequuntur expedita et cum reprehenderit molestiae ut ut quas totam nostrum rerum est autem sunt rem eveniet architecto")
            .build();

        final User user = User.builder()
            .id(1)
            .email("Sincere@april.biz")
            .userName("Bret")
            .build();

        return PostDetails.create(post, user, commentCount);
    }
}
