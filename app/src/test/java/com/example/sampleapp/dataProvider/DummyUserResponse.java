package com.example.sampleapp.dataProvider;

import android.support.annotation.NonNull;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import com.example.sampleapp.data.network.UserResponse;


public class DummyUserResponse {

    @NonNull
    public static List<UserResponse> create() {
        final UserResponse first = UserResponse.builder()
            .id(1)
            .email("Sincere@april.biz")
            .userName("Bret")
            .build();

        final UserResponse second = UserResponse.builder()
            .id(2)
            .email("Shanna@melissa.tv")
            .userName("Antonette")
            .build();

        return Collections.unmodifiableList(Arrays.asList(first, second));
    }
}
