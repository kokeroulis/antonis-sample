package com.example.sampleapp.dataProvider;


import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import com.example.sampleapp.data.network.PostResponse;

public class DummyPostReponse {

    public static List<PostResponse> create() {
        final PostResponse first = PostResponse.builder()
            .id(1)
            .userId(1)
            .title("sunt aut facere repellat provident occaecati excepturi optio reprehenderit")
            .body("quia et suscipit suscipit recusandae consequuntur expedita et cum reprehenderit molestiae ut ut quas totam nostrum rerum est autem sunt rem eveniet architecto")
            .build();

        final PostResponse second = PostResponse.builder()
            .id(2)
            .userId(2)
            .title("qui est esse")
            .body("est rerum tempore vitae sequi sint nihil reprehenderit dolor beatae ea dolores neque fugiat blanditiis voluptate porro vel nihil molestiae ut reiciendis qui aperiam non debitis possimus qui neque nisi nulla")
            .build();

        return Collections.unmodifiableList(Arrays.asList(first, second));
    }
}
