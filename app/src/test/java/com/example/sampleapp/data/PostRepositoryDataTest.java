package com.example.sampleapp.data;

import android.support.annotation.NonNull;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import com.example.sampleapp.base.BaseRxTest;
import com.example.sampleapp.data.entity.PostEntity;
import com.example.sampleapp.data.entity.mapper.PostMapper;
import com.example.sampleapp.data.entity.mapper.PostResponseMapper;
import com.example.sampleapp.data.network.PostResponse;
import com.example.sampleapp.data.repository.post.PostDataRepository;
import com.example.sampleapp.data.repository.post.PostDataStore;
import com.example.sampleapp.dataProvider.DummyPostReponse;
import com.example.sampleapp.domain.models.Post;
import io.reactivex.Completable;
import io.reactivex.processors.BehaviorProcessor;
import io.reactivex.subscribers.TestSubscriber;

public class PostRepositoryDataTest extends BaseRxTest {

    @Mock
    PostDataStore localDataStore;

    @Mock
    PostDataStore remoteDataStore;

    private PostDataRepository repository;
    private PostResponseMapper postResponseMapper;
    private PostMapper postMapper;

    @Before
    public void before() {
        postMapper = new PostMapper();
        repository = new PostDataRepository(localDataStore, remoteDataStore, postMapper);
        postResponseMapper = new PostResponseMapper();
    }

    @Test
    public void getAllPosts() {
        List<PostEntity> postEntityList = createPostEntityList();

        new StreamBuilder().emitPosts(localDataStore, postEntityList);
        TestSubscriber<List<Post>> testSubscriber = repository.getPosts().test();
        testSubscriber.assertNotComplete();
        testSubscriber.assertNoErrors();

        testSubscriber.assertValue(postMapper.transformToPost(postEntityList));
    }

    @Test
    public void getPostsByIdWhenItExists() {
        List<PostEntity> postEntityList = createPostEntityList();

        new StreamBuilder().emitPostForId(localDataStore, postEntityList);
        TestSubscriber<Post> testSubscriber = repository.findPostById(1).test();
        testSubscriber.assertNotComplete();
        testSubscriber.assertNoErrors();

        testSubscriber.assertValue(postMapper.transform(postEntityList.get(0)));
    }

    @Test
    public void getPostsByIdAndItDoesnNotExists() {
        new StreamBuilder().emitPostForIdAndItDoesntExist(localDataStore);
        TestSubscriber<Post> testSubscriber = repository.findPostById(1).test();
        testSubscriber.assertNotComplete();
        testSubscriber.assertNoErrors();
        testSubscriber.assertNoValues();
    }

    @Test
    public void getPostFromCloudAndSave() {
        List<PostEntity> postEntityList = createPostEntityList();

        new StreamBuilder().emitPostResponsesFromCloud(localDataStore, remoteDataStore, postEntityList);
        repository.forceUpdate().test().assertNoErrors();
    }

    @NonNull
    private List<PostEntity> createPostEntityList() {
        List<PostEntity> postEntityList = new ArrayList<>();
        for (PostResponse response : DummyPostReponse.create()) {
            postEntityList.add(postResponseMapper.transform(response));
        }

        return Collections.unmodifiableList(postEntityList);
    }

    private static class StreamBuilder {
        private final BehaviorProcessor<List<PostEntity>> repoStream = BehaviorProcessor.create();

        private void emitPosts(@NonNull PostDataStore localDataStore, @NonNull List<PostEntity> postList) {
            Mockito.when(localDataStore.getPosts()).thenReturn(repoStream);
            repoStream.onNext(postList);
        }

        private void emitPostForId(@NonNull PostDataStore localDataStore, @NonNull List<PostEntity> postList) {
            final List<PostEntity> singleItem = Arrays.asList(postList.get(0));
            Mockito.when(localDataStore.getPostById(1)).thenReturn(repoStream);
            repoStream.onNext(singleItem);
        }

        private void emitPostForIdAndItDoesntExist(PostDataStore localDataStore) {
            Mockito.when(localDataStore.getPostById(1)).thenReturn(repoStream);
        }

        private void emitPostResponsesFromCloud(@NonNull PostDataStore localDataStore,
                                                @NonNull PostDataStore remoteDataStore,
                                                @NonNull List<PostEntity> responseList) {

            Mockito.when(localDataStore.savePosts(Mockito.anyList())).thenReturn(Completable.complete());
            Mockito.when(remoteDataStore.getPosts()).thenReturn(repoStream);
            repoStream.onNext(responseList);
        }
    }
}
