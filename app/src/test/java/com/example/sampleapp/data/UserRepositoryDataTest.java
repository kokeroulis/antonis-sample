package com.example.sampleapp.data;

import android.support.annotation.NonNull;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.example.sampleapp.base.BaseRxTest;
import com.example.sampleapp.data.entity.UserEntity;
import com.example.sampleapp.data.entity.mapper.UserMapper;
import com.example.sampleapp.data.entity.mapper.UserResponseMapper;
import com.example.sampleapp.data.network.UserResponse;
import com.example.sampleapp.data.repository.user.UserDataRepository;
import com.example.sampleapp.data.repository.user.UserDataStore;
import com.example.sampleapp.dataProvider.DummyUserResponse;
import com.example.sampleapp.domain.models.User;
import io.reactivex.Completable;
import io.reactivex.processors.BehaviorProcessor;
import io.reactivex.subscribers.TestSubscriber;
import polanski.option.Option;

public class UserRepositoryDataTest extends BaseRxTest {

    @Mock
    UserDataStore localDataStore;

    @Mock
    UserDataStore remoteDataStore;

    private UserDataRepository repository;

    private UserResponseMapper responseMapper;

    private UserMapper userMapper;

    @Before
    public void before() {
        userMapper = new UserMapper();
        responseMapper = new UserResponseMapper();
        repository = new UserDataRepository(localDataStore, remoteDataStore, userMapper);
    }

    @Test
    public void getAllUsers() {
        List<UserEntity> userEntityList = createUserEntityList();

        new StreamBuilder().emitUsers(localDataStore, userEntityList);
        TestSubscriber<List<User>> testSubscriber = repository.getUsers().test();
        testSubscriber.assertNotComplete();
        testSubscriber.assertNoErrors();

        testSubscriber.assertValue(userMapper.transform(userEntityList));
    }

    @Test
    public void getPostsByIdWhenItExists() {
        List<UserEntity> userEntityList = createUserEntityList();

        new StreamBuilder().emitUserForId(localDataStore, userEntityList);
        TestSubscriber<Option<User>> testSubscriber = repository.getUserById(1).test();
        testSubscriber.assertNotComplete();
        testSubscriber.assertNoErrors();

        final User user = userMapper.transform(userEntityList.get(0));
        testSubscriber.assertValue(Option.ofObj(user));
    }

    @Test
    public void getPostsByIdAndItDoesnNotExists() {
        new StreamBuilder().emitUserForIdAndItDoesntExist(localDataStore);
        TestSubscriber<Option<User>> testSubscriber = repository.getUserById(1).test();
        testSubscriber.assertNotComplete();
        testSubscriber.assertNoErrors();
        testSubscriber.assertNoValues();
    }

    @Test
    public void getPostFromCloudAndSave() {
        List<UserEntity> userEntityList = createUserEntityList();
        new StreamBuilder().emitUserResponsesFromCloud(localDataStore, remoteDataStore, userEntityList);
        repository.forceUpdate().test().assertNoErrors();
    }

    @NonNull
    private List<UserEntity> createUserEntityList() {
        List<UserEntity> userEntityList = new ArrayList<>();
        for (UserResponse response : DummyUserResponse.create()) {
            userEntityList.add(responseMapper.transform(response));
        }

        return Collections.unmodifiableList(userEntityList);
    }

    private static class StreamBuilder {
        private final BehaviorProcessor<List<UserEntity>> repoStream = BehaviorProcessor.create();

        private void emitUsers(@NonNull UserDataStore localDataStore, @NonNull List<UserEntity> userList) {
            Mockito.when(localDataStore.getUsers()).thenReturn(repoStream);
            repoStream.onNext(userList);
        }

        private void emitUserForId(@NonNull UserDataStore localDataStore, @NonNull List<UserEntity> userList) {
            final BehaviorProcessor<Option<UserEntity>> stream = BehaviorProcessor.create();
            Mockito.when(localDataStore.getUserById(1)).thenReturn(stream);
            stream.onNext(Option.ofObj(userList.get(0)));
        }

        private void emitUserForIdAndItDoesntExist(@NonNull UserDataStore localDataStore) {
            final BehaviorProcessor<Option<UserEntity>> stream = BehaviorProcessor.create();
            Mockito.when(localDataStore.getUserById(1)).thenReturn(stream);
        }

        private void emitUserResponsesFromCloud(@NonNull UserDataStore localDataStore,
                                                @NonNull UserDataStore remoteDataStore,
                                                @NonNull List<UserEntity> responseList) {

            Mockito.when(localDataStore.saveUsers(Mockito.anyList())).thenReturn(Completable.complete());
            Mockito.when(remoteDataStore.getUsers()).thenReturn(repoStream);
            repoStream.onNext(responseList);
        }
    }
}
