package com.example.sampleapp.data;

import android.support.annotation.NonNull;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.example.sampleapp.base.BaseRxTest;
import com.example.sampleapp.data.entity.CommentEntity;
import com.example.sampleapp.data.entity.mapper.CommentMapper;
import com.example.sampleapp.data.entity.mapper.CommentResponseMapper;
import com.example.sampleapp.data.network.CommentResponse;
import com.example.sampleapp.data.repository.comment.CommentDataRepository;
import com.example.sampleapp.data.repository.comment.CommentDataStore;
import com.example.sampleapp.dataProvider.DummyCommentsResponse;
import com.example.sampleapp.domain.models.Comment;
import io.reactivex.Completable;
import io.reactivex.processors.BehaviorProcessor;
import io.reactivex.subscribers.TestSubscriber;

public class CommentRepositoryDataTest extends BaseRxTest {

    @Mock
    CommentDataStore localDataStore;

    @Mock
    CommentDataStore remoteDataStore;

    private CommentDataRepository repository;

    private CommentResponseMapper responseMapper;

    private CommentMapper commentMapper;

    @Before
    public void before() {
        commentMapper = new CommentMapper();
        responseMapper = new CommentResponseMapper();
        repository = new CommentDataRepository(localDataStore, remoteDataStore, commentMapper);
    }

    @Test
    public void getPostsByIdWhenItExists() {
        List<CommentEntity> commentEntityList = createCommentEntityList();

        new StreamBuilder().emitCommentForId(localDataStore, commentEntityList);
        TestSubscriber<List<Comment>> testSubscriber = repository.getCommentByPostId(1).test();
        testSubscriber.assertNotComplete();
        testSubscriber.assertNoErrors();

        testSubscriber.assertValue(commentMapper.transform(commentEntityList));
    }

    @Test
    public void getPostsByIdAndItDoesnNotExists() {
        new StreamBuilder().emitCommentForIdAndItDoesntExist(localDataStore);
        TestSubscriber<List<Comment>> testSubscriber = repository.getCommentByPostId(1).test();
        testSubscriber.assertNotComplete();
        testSubscriber.assertNoErrors();
        testSubscriber.assertNoValues();
    }

    @Test
    public void getPostFromCloudAndSave() {
        List<CommentEntity> commentEntityList = createCommentEntityList();
        new StreamBuilder().emitCommentResponsesFromCloud(localDataStore, remoteDataStore, commentEntityList);
        repository.forceUpdate().test().assertNoErrors();
    }

    @NonNull
    private List<CommentEntity> createCommentEntityList() {
        List<CommentEntity> commentEntityList = new ArrayList<>();
        for (CommentResponse response : DummyCommentsResponse.create()) {
            commentEntityList.add(responseMapper.transform(response));
        }

        return Collections.unmodifiableList(commentEntityList);
    }

    private static class StreamBuilder {
        private final BehaviorProcessor<List<CommentEntity>> repoStream = BehaviorProcessor.create();

        private void emitCommentForId(@NonNull CommentDataStore localDataStore, @NonNull List<CommentEntity> commentList) {
            Mockito.when(localDataStore.getCommentByPostId(1)).thenReturn(repoStream);
            repoStream.onNext(commentList);
        }

        private void emitCommentForIdAndItDoesntExist(@NonNull CommentDataStore localDataStore) {
            final BehaviorProcessor<List<CommentEntity>> stream = BehaviorProcessor.create();
            Mockito.when(localDataStore.getCommentByPostId(1)).thenReturn(stream);
        }

        private void emitCommentResponsesFromCloud(@NonNull CommentDataStore localDataStore,
                                                   @NonNull CommentDataStore remoteDataStore,
                                                   @NonNull List<CommentEntity> responseList) {

            Mockito.when(localDataStore.saveComments(Mockito.anyList())).thenReturn(Completable.complete());
            Mockito.when(remoteDataStore.getComments()).thenReturn(repoStream);
            repoStream.onNext(responseList);
        }
    }
}
