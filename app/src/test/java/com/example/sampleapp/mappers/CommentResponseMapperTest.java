package com.example.sampleapp.mappers;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.example.sampleapp.base.CustomRobolectricTestRunner;
import com.example.sampleapp.data.entity.CommentEntity;
import com.example.sampleapp.data.entity.mapper.CommentResponseMapper;
import com.example.sampleapp.data.network.CommentResponse;
import com.example.sampleapp.dataProvider.DummyCommentsResponse;

import static org.assertj.core.api.Assertions.assertThat;


@RunWith(CustomRobolectricTestRunner.class)
public class CommentResponseMapperTest {

    private CommentResponseMapper commentResponseMapper;

    @Before
    public void before() {
        commentResponseMapper = new CommentResponseMapper();
    }

    @Test
    public void testCommentEntityMapperOk() {
        final CommentResponse response = DummyCommentsResponse.create().get(0);
        final CommentEntity commentEntity = commentResponseMapper.transform(response);

        assertThat(commentEntity.getId()).isEqualTo(1);
        assertThat(commentEntity.getPostId()).isEqualTo(1);
        assertThat(commentEntity.getBody()).isEqualTo("some body 111");
        assertThat(commentEntity.getEmail()).isEqualTo("Eliseo@gardner.biz");
        assertThat(commentEntity.getName()).isEqualTo("id labore ex et quam laborum");
    }
}
