package com.example.sampleapp.mappers;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.ArrayList;
import java.util.List;

import com.example.sampleapp.base.CustomRobolectricTestRunner;
import com.example.sampleapp.data.entity.CommentEntity;
import com.example.sampleapp.data.entity.mapper.CommentMapper;
import com.example.sampleapp.data.entity.mapper.CommentResponseMapper;
import com.example.sampleapp.data.network.CommentResponse;
import com.example.sampleapp.dataProvider.DummyComment;
import com.example.sampleapp.dataProvider.DummyCommentsResponse;
import com.example.sampleapp.domain.models.Comment;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(CustomRobolectricTestRunner.class)
public class CommentMapperTest {

    private CommentResponseMapper commentResponseMapper;
    private CommentMapper commentMapper;

    @Before
    public void before() {
        commentResponseMapper = new CommentResponseMapper();
        commentMapper = new CommentMapper();
    }

    @Test
    public void testCommentMapperOk() {
        List<CommentEntity> commentEntityList = new ArrayList<>();
        for (CommentResponse response : DummyCommentsResponse.create()) {
            commentEntityList.add(commentResponseMapper.transform(response));
        }

        List<Comment> commentList = commentMapper.transform(commentEntityList);
        assertThat(commentList).isEqualTo(DummyComment.create());
    }
}
