package com.example.sampleapp.mappers;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;

import java.util.ArrayList;
import java.util.List;

import com.example.sampleapp.base.CustomRobolectricTestRunner;
import com.example.sampleapp.data.entity.UserEntity;
import com.example.sampleapp.data.entity.mapper.UserMapper;
import com.example.sampleapp.data.entity.mapper.UserResponseMapper;
import com.example.sampleapp.data.network.UserResponse;
import com.example.sampleapp.dataProvider.DummyUser;
import com.example.sampleapp.dataProvider.DummyUserResponse;
import com.example.sampleapp.domain.models.User;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(CustomRobolectricTestRunner.class)
public class UserMapperTest {

    private UserResponseMapper userResponseMapper;
    private UserMapper userMapper;

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Before
    public void before() {
        userResponseMapper = new UserResponseMapper();
        userMapper = new UserMapper();
    }

    @Test
    public void testUserMapperOk() {
        List<UserEntity> userEntityList = new ArrayList<>();
        for (UserResponse response : DummyUserResponse.create()) {
            userEntityList.add(userResponseMapper.transform(response));
        }

        List<User> userList = userMapper.transform(userEntityList);
        assertThat(userList).isEqualTo(DummyUser.create());
    }

    // These test exist so we are sure that we have not annotated something
    // with @Nullable where we shouldn't
    @Test
    public void testWrongEmailUserModel() {
        thrown.expect(NullPointerException.class);
        thrown.expectMessage("Null email");

        User.builder()
            .id(1)
            .email(null)
            .userName("asddsa")
            .build();
    }

    @Test
    public void testWrongUserNameUserModel() {
        thrown.expect(NullPointerException.class);
        thrown.expectMessage("Null userName");

        User.builder()
            .id(1)
            .email("foo@foo.gr")
            .userName(null)
            .build();
    }
}
