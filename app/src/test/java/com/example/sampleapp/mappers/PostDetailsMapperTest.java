package com.example.sampleapp.mappers;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;

import com.example.sampleapp.base.CustomRobolectricTestRunner;
import com.example.sampleapp.dataProvider.DummyPost;
import com.example.sampleapp.dataProvider.DummyPostDetails;
import com.example.sampleapp.dataProvider.DummyPostDetailsModel;
import com.example.sampleapp.dataProvider.DummyPostModel;
import com.example.sampleapp.dataProvider.DummyUser;
import com.example.sampleapp.dataProvider.DummyUserModel;
import com.example.sampleapp.domain.models.Post;
import com.example.sampleapp.domain.models.PostDetails;
import com.example.sampleapp.domain.models.User;
import com.example.sampleapp.presentation.model.PostDetailsMapper;
import com.example.sampleapp.presentation.model.PostDetailsModel;
import com.example.sampleapp.presentation.model.PostModel;
import com.example.sampleapp.presentation.model.PostModelMapper;
import com.example.sampleapp.presentation.model.UserModel;
import com.example.sampleapp.presentation.model.UserModelMapper;

import static org.assertj.core.api.AssertionsForInterfaceTypes.assertThat;

@RunWith(CustomRobolectricTestRunner.class)
public class PostDetailsMapperTest {

    private UserModelMapper userModelMapper;
    private PostModelMapper postModelMapper;
    private PostDetailsMapper postDetailsMapper;
    private static final int COMMENT_COUNT = 10;

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Before
    public void before() {
        userModelMapper = new UserModelMapper();
        postModelMapper = new PostModelMapper();
        postDetailsMapper = new PostDetailsMapper(userModelMapper, postModelMapper);
    }

    @Test
    public void testPostDetailsModelMapperOk() {
        final User user = DummyUser.create().get(0);
        final Post post = DummyPost.create().get(0);

        final PostDetails model = PostDetails.create(post, user, COMMENT_COUNT);
        assertThat(model).isEqualTo(DummyPostDetails.create(COMMENT_COUNT));

        final PostDetailsModel mappedModel = postDetailsMapper.transform(model);
        assertThat(mappedModel).isEqualTo(DummyPostDetailsModel.create(COMMENT_COUNT));
    }

    // These test exist so we are sure that we have not annotated something
    // with @Nullable where we shouldn't
    @Test
    public void testWrongPostDetailsPostMissing() {
        thrown.expect(NullPointerException.class);
        thrown.expectMessage("Null post");

        final User user = DummyUser.create().get(0);
        PostDetails.create(null, user, COMMENT_COUNT);
    }

    @Test
    public void testWrongPostDetailsUserMissing() {
        thrown.expect(NullPointerException.class);
        thrown.expectMessage("Null user");

        final Post post = DummyPost.create().get(0);
        PostDetails.create(post, null, COMMENT_COUNT);
    }

    @Test
    public void testWrongPostDetailsModelPostMissing() {
        thrown.expect(NullPointerException.class);
        thrown.expectMessage("Null post");

        final UserModel user = DummyUserModel.create().get(0);
        PostDetailsModel.builder()
            .postModel(null)
            .userModel(user)
            .commentCount(COMMENT_COUNT)
            .build();
    }

    @Test
    public void testWrongPostDetailsModelUserMissing() {
        thrown.expect(NullPointerException.class);
        thrown.expectMessage("Null user");

        final UserModel user = DummyUserModel.create().get(0);
        final PostModel post = DummyPostModel.create(user.email()).get(0);
        PostDetailsModel.builder()
            .postModel(post)
            .userModel(null)
            .commentCount(COMMENT_COUNT)
            .build();
    }
}
