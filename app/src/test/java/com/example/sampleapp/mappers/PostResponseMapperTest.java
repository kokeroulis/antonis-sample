package com.example.sampleapp.mappers;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.example.sampleapp.base.CustomRobolectricTestRunner;
import com.example.sampleapp.data.entity.PostEntity;
import com.example.sampleapp.data.entity.mapper.PostResponseMapper;
import com.example.sampleapp.data.network.PostResponse;
import com.example.sampleapp.dataProvider.DummyPostReponse;

import static org.assertj.core.api.Assertions.assertThat;


@RunWith(CustomRobolectricTestRunner.class)
public class PostResponseMapperTest {

    private PostResponseMapper postResponseMapper;

    @Before
    public void before() {
        postResponseMapper = new PostResponseMapper();
    }

    @Test
    public void testPostEntityMapperOk() {
        final PostResponse response = DummyPostReponse.create().get(0);
        final PostEntity postEntity = postResponseMapper.transform(response);

        assertThat(postEntity.getId()).isEqualTo(1);
        assertThat(postEntity.getUserId()).isEqualTo(1);
        assertThat(postEntity.getTitle()).isEqualTo("sunt aut facere repellat provident occaecati excepturi optio reprehenderit");
        assertThat(postEntity.getBody()).isEqualTo("quia et suscipit suscipit recusandae consequuntur expedita et cum reprehenderit molestiae ut ut quas totam nostrum rerum est autem sunt rem eveniet architecto");
    }
}
