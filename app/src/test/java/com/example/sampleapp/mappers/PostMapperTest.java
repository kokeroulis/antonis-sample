package com.example.sampleapp.mappers;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;

import java.util.ArrayList;
import java.util.List;

import com.example.sampleapp.base.CustomRobolectricTestRunner;
import com.example.sampleapp.data.entity.PostEntity;
import com.example.sampleapp.data.entity.mapper.PostMapper;
import com.example.sampleapp.data.entity.mapper.PostResponseMapper;
import com.example.sampleapp.data.network.PostResponse;
import com.example.sampleapp.dataProvider.DummyPost;
import com.example.sampleapp.dataProvider.DummyPostReponse;
import com.example.sampleapp.domain.models.Post;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(CustomRobolectricTestRunner.class)
public class PostMapperTest {

    private PostResponseMapper postResponseMapper;
    private PostMapper postMapper;

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Before
    public void before() {
        postResponseMapper = new PostResponseMapper();
        postMapper = new PostMapper();
    }

    @Test
    public void testPostMapperOk() {
        List<PostEntity> postEntityList = new ArrayList<>();
        for (PostResponse response : DummyPostReponse.create()) {
            postEntityList.add(postResponseMapper.transform(response));
        }

        List<Post> postList = postMapper.transformToPost(postEntityList);
        assertThat(postList).isEqualTo(DummyPost.create());
    }

    // These test exist so we are sure that we have not annotated something
    // with @Nullable where we shouldn't
    @Test
    public void testWrongTitlePostModel() {
        thrown.expect(NullPointerException.class);
        thrown.expectMessage("Null title");

        Post.builder()
            .id(1)
            .userId(1)
            .title(null)
            .body("dummy")
            .build();
    }

    @Test
    public void testWrongBodyPostModel() {
        thrown.expect(NullPointerException.class);
        thrown.expectMessage("Null body");

        Post.builder()
            .id(1)
            .userId(1)
            .title("dummy")
            .body(null)
            .build();
    }
}
