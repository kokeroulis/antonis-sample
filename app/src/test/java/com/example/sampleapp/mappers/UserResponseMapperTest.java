package com.example.sampleapp.mappers;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.example.sampleapp.base.CustomRobolectricTestRunner;
import com.example.sampleapp.data.entity.UserEntity;
import com.example.sampleapp.data.entity.mapper.UserResponseMapper;
import com.example.sampleapp.data.network.UserResponse;
import com.example.sampleapp.dataProvider.DummyUserResponse;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(CustomRobolectricTestRunner.class)
public class UserResponseMapperTest {
    private UserResponseMapper userResponseMapper;

    @Before
    public void before() {
        userResponseMapper = new UserResponseMapper();
    }

    @Test
    public void testCommentEntityMapperOk() {
        final UserResponse response = DummyUserResponse.create().get(0);
        final UserEntity userEntity = userResponseMapper.transform(response);

        assertThat(userEntity.getId()).isEqualTo(1);
        assertThat(userEntity.getEmail()).isEqualTo("Sincere@april.biz");
        assertThat(userEntity.getUserName()).isEqualTo("Bret");
    }
}
