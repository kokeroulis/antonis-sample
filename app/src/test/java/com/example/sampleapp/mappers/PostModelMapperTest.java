package com.example.sampleapp.mappers;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;

import java.util.Arrays;
import java.util.List;

import com.example.sampleapp.base.CustomRobolectricTestRunner;
import com.example.sampleapp.dataProvider.DummyPost;
import com.example.sampleapp.dataProvider.DummyPostModel;
import com.example.sampleapp.dataProvider.DummyUser;
import com.example.sampleapp.domain.models.Post;
import com.example.sampleapp.domain.models.PostWithUserAvatar;
import com.example.sampleapp.domain.models.User;
import com.example.sampleapp.presentation.model.PostModel;
import com.example.sampleapp.presentation.model.PostModelMapper;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(CustomRobolectricTestRunner.class)
public class PostModelMapperTest {

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    private PostModelMapper postModelMapper;

    @Before
    public void before() {
        postModelMapper = new PostModelMapper();
    }

    @Test
    public void testPostModelMapperFromPostOk() {
        final Post post = DummyPost.create().get(0);
        final User user = DummyUser.create().get(0);

        final PostModel postModel = postModelMapper.transformFromPost(post, user.email());
        final PostModel expected = DummyPostModel.create(user.email()).get(0);

        assertThat(postModel).isEqualTo(expected);
    }

    @Test
    public void testPostModelMapperFromPostWithUserAvatarOk() {
        final Post post = DummyPost.create().get(0);
        final User user = DummyUser.create().get(0);
        final PostWithUserAvatar postWithUserAvatar = PostWithUserAvatar.create(post, user.email());

        final PostModel postModel = postModelMapper.transform(postWithUserAvatar);
        final PostModel expected = DummyPostModel.create(user.email()).get(0);

        assertThat(postModel).isEqualTo(expected);

        final List<PostWithUserAvatar> postWithUserAvatarList = Arrays.asList(postWithUserAvatar, postWithUserAvatar);
        final List<PostModel> expectedPostList = Arrays.asList(expected, expected);

        assertThat(postModelMapper.transform(postWithUserAvatarList)).isEqualTo(expectedPostList);
    }

    // These test exist so we are sure that we have not annotated something
    // with @Nullable where we shouldn't
    @Test
    public void testWrongPostMissingFromPostWithUserAvatarModel() {
        thrown.expect(NullPointerException.class);
        thrown.expectMessage("Null post");

        PostWithUserAvatar.create(null, "foo@bar.com");
    }
}
