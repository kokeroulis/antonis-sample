package com.example.sampleapp.mappers;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;

import com.example.sampleapp.base.CustomRobolectricTestRunner;
import com.example.sampleapp.dataProvider.DummyUser;
import com.example.sampleapp.dataProvider.DummyUserModel;
import com.example.sampleapp.domain.models.User;
import com.example.sampleapp.presentation.model.UserModel;
import com.example.sampleapp.presentation.model.UserModelMapper;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(CustomRobolectricTestRunner.class)
public class UserModelMapperTest {

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    private UserModelMapper userModelMapper;

    @Before
    public void before() {
        userModelMapper = new UserModelMapper();
    }

    @Test
    public void testUserModelMapperOk() {
        final User user  = DummyUser.create().get(0);
        final UserModel expectedUserModel = DummyUserModel.create().get(0);

        assertThat(expectedUserModel).isEqualTo(userModelMapper.transform(user));
    }

    // These test exist so we are sure that we have not annotated something
    // with @Nullable where we shouldn't
    @Test
    public void testWrongEmailUserModel() {
        thrown.expect(NullPointerException.class);
        thrown.expectMessage("Null email");

        UserModel.builder()
            .id(1)
            .email(null)
            .userName("foo")
            .build();
    }

    @Test
    public void testWrongUserNameUserModel() {
        thrown.expect(NullPointerException.class);
        thrown.expectMessage("Null userName");

        UserModel.builder()
            .id(1)
            .email("foo@foo.gr")
            .userName(null)
            .build();
    }
}
