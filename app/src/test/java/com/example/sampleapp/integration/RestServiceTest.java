package com.example.sampleapp.integration;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.io.IOException;
import java.util.List;

import com.example.sampleapp.base.CustomRobolectricTestRunner;
import com.example.sampleapp.base.UnitTestApplication;
import com.example.sampleapp.data.network.CommentResponse;
import com.example.sampleapp.data.network.PostResponse;
import com.example.sampleapp.data.network.RestService;
import com.example.sampleapp.data.network.UserResponse;
import com.example.sampleapp.dataProvider.DummyCommentsResponse;
import com.example.sampleapp.dataProvider.DummyPostReponse;
import com.example.sampleapp.dataProvider.DummyUserResponse;
import com.example.sampleapp.helpers.MockServerSetupHelper;
import okhttp3.mockwebserver.MockWebServer;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(CustomRobolectricTestRunner.class)
public class RestServiceTest {

    private final MockServerSetupHelper mockServerSetupHelper = new MockServerSetupHelper();
    private RestService restService;
    private MockWebServer mockWebServer;

    @Before
    public void before() throws IOException {
        mockWebServer = mockServerSetupHelper.createAndSetupMockWebServer();
        restService = UnitTestApplication.getApplicationComponent().restService();
    }

    @Test
    public void testPostResponseSuccess() throws IOException {
        mockServerSetupHelper.enqueueJsonAssetToMockWebServer("post_response.json", mockWebServer);

        final List<PostResponse> responseList = restService.getPostList().toObservable().blockingFirst();
        assertThat(responseList).isEqualTo(DummyPostReponse.create());
    }

    @Test
    public void testUserResponseSuccess() throws IOException {
        mockServerSetupHelper.enqueueJsonAssetToMockWebServer("users_response.json", mockWebServer);

        final List<UserResponse> responseList = restService.getUserList().toObservable().blockingFirst();
        assertThat(responseList).isEqualTo(DummyUserResponse.create());
    }

    @Test
    public void testCommentResponseSuccess() throws IOException {
        mockServerSetupHelper.enqueueJsonAssetToMockWebServer("comments_response.json", mockWebServer);

        final List<CommentResponse> responseList = restService.getCommentList().toObservable().blockingFirst();
        assertThat(responseList).isEqualTo(DummyCommentsResponse.create());
    }

    @After
    public void after() throws IOException {
        mockWebServer.shutdown();
    }

}
