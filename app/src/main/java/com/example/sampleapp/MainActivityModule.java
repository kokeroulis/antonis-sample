package com.example.sampleapp;

import android.support.v4.app.FragmentManager;

import dagger.Module;
import dagger.Provides;
import dagger.android.ContributesAndroidInjector;
import com.example.sampleapp.dagger.scopes.ActivityScope;
import com.example.sampleapp.dagger.scopes.FragmentScope;
import com.example.sampleapp.presentation.post.PostFragment;
import com.example.sampleapp.presentation.post.PostModule;
import com.example.sampleapp.presentation.postDetails.PostDetailsFragment;
import com.example.sampleapp.presentation.postDetails.PostDetailsModule;

@Module
public abstract class MainActivityModule {

    @FragmentScope
    @ContributesAndroidInjector(modules = PostModule.class)
    abstract PostFragment contributesPostFragmentInjector();

    @FragmentScope
    @ContributesAndroidInjector(modules = PostDetailsModule.class)
    abstract PostDetailsFragment contributesPostDetailsFragmentInjector();

    // MainActivity has been bounded by the ContributesAndroidInjector
    // annotation, there is no need to bound it
    @Provides
    @ActivityScope
    static FragmentManager activityFragmentManager(MainActivity activity) {
        return activity.getSupportFragmentManager();
    }
}
