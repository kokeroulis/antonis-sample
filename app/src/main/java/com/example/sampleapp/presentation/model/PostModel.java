package com.example.sampleapp.presentation.model;

import android.support.annotation.NonNull;

import com.google.auto.value.AutoValue;

@AutoValue
public abstract class PostModel {
    public abstract int id();
    public abstract int userId();
    public abstract String title();
    public abstract String body();
    public abstract String avatarUrl();

    @NonNull
    public static Builder builder() {
        return new AutoValue_PostModel.Builder();
    }

    @AutoValue.Builder
    public interface Builder {
        Builder id(int id);
        Builder userId(int userId);
        Builder title(String title);
        Builder body(String body);
        Builder avatarUrl(String avatarUrl);
        PostModel build();
    }
}