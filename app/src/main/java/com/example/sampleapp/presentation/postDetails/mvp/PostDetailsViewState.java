package com.example.sampleapp.presentation.postDetails.mvp;

import android.support.annotation.Nullable;

import com.hannesdorfmann.mosby3.mvp.viewstate.ViewState;

import com.example.sampleapp.presentation.model.PostDetailsModel;
import timber.log.Timber;

public class PostDetailsViewState implements ViewState<PostDetailsView> {

    @Nullable
    private PostDetailsModel postDetailsModel;

    @Override
    public void apply(PostDetailsView view, boolean retained) {
        if (postDetailsModel != null) {
            Timber.d("Restoring from the view state");
            view.hideLoadingBar();
            view.loadPostDetails(postDetailsModel);
        }
    }

    public void setPostDetailsModel(@Nullable PostDetailsModel postDetailsModel) {
        this.postDetailsModel = postDetailsModel;
    }
}
