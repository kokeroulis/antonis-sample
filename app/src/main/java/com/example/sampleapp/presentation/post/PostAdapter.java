package com.example.sampleapp.presentation.post;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.jakewharton.rxbinding2.view.RxView;

import java.util.Collections;
import java.util.List;
import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;
import com.example.sampleapp.R;
import com.example.sampleapp.presentation.model.PostModel;
import com.example.sampleapp.presentation.post.PostAdapter.PostViewHolder;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.subjects.PublishSubject;
import timber.log.Timber;

public class PostAdapter extends RecyclerView.Adapter<PostViewHolder> {
    @NonNull
    private List<PostModel> postModelList = Collections.emptyList();
    private final PublishSubject<PostModel> modelSubject = PublishSubject.create();
    private final CompositeDisposable compositeDisposable;

    public PostAdapter(CompositeDisposable compositeDisposable) {
        this.compositeDisposable = compositeDisposable;
    }

    @Override
    public PostViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.holder_post, parent, false);
        return new PostViewHolder(view, modelSubject);
    }

    @Override
    public void onBindViewHolder(PostViewHolder holder, int position) {
        final PostModel model = postModelList.get(position);
        holder.bind(model, postModelList, compositeDisposable);
    }

    @Override
    public int getItemCount() {
        return postModelList.size();
    }

    public void setPostModelList(@NonNull List<PostModel> postModelList) {
        this.postModelList = postModelList;
    }

    static class PostViewHolder extends RecyclerView.ViewHolder {
        private final PublishSubject<PostModel> modelSubject;

        @BindView(R.id.title) TextView titleView;
        @BindView(R.id.rootCard) LinearLayout rootCardView;
        @BindView(R.id.avatar) ImageView avatarView;

        public PostViewHolder(@NonNull View itemView, @NonNull PublishSubject<PostModel> modelSubject) {
            super(itemView);
            this.modelSubject = modelSubject;
            ButterKnife.bind(this, itemView);
        }

        void bind(@NonNull PostModel model, @NonNull List<PostModel> postModelList,
                  CompositeDisposable compositeDisposable) {
            titleView.setText(model.title());
            Glide.with(avatarView)
                .load(model.avatarUrl())
                .into(avatarView);

            // This is a bit of an overkill for this example
            // But we want to enforce separation between the ViewHolder
            // and the Adapter. We could use just a callback inside the
            // onBindViewHolder
            compositeDisposable.add(RxView.clicks(rootCardView)
                .debounce(250, TimeUnit.MILLISECONDS)
                .observeOn(AndroidSchedulers.mainThread()) // move back to the main thread
                .map(ignore -> {
                    final int adapterPos = getAdapterPosition();
                    return postModelList.get(adapterPos);
                })
                .subscribe(modelSubject::onNext, error -> Timber.e(error, "We had an error"))
            );
        }
    }

    public Observable<PostModel> getModelSubject() {
        return modelSubject;
    }
}
