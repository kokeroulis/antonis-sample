package com.example.sampleapp.presentation.post;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.LayoutManager;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindString;
import butterknife.BindView;
import com.example.sampleapp.R;
import com.example.sampleapp.base.BaseMvpFragment;
import com.example.sampleapp.presentation.Navigator;
import com.example.sampleapp.presentation.model.PostModel;
import com.example.sampleapp.presentation.post.mvp.PostPresenter;
import com.example.sampleapp.presentation.post.mvp.PostView;
import com.example.sampleapp.presentation.post.mvp.PostViewState;
import com.example.sampleapp.presentation.postDetails.PostDetailsFragment;
import io.reactivex.disposables.CompositeDisposable;
import timber.log.Timber;

public class PostFragment extends BaseMvpFragment<PostView, PostPresenter, PostViewState>
    implements PostView {

    @BindView(R.id.rv) RecyclerView recyclerView;
    @BindView(R.id.progressBar) ProgressBar progressBarView;
    @BindString(R.string.post_screen_title) String postScreenTitle;

    @Nullable CompositeDisposable compositeDisposable;

    public static final String TAG = PostFragment.class.getSimpleName();

    @Inject Navigator navigator;

    private PostAdapter adapter;

    @Override
    public void onNewViewStateInstance() {
        super.onNewViewStateInstance();
        presenter.loadPosts();
    }

    @NonNull
    @Override
    public PostViewState createViewState() {
        return new PostViewState();
    }

    @Override
    protected int getLayoutRes() {
        return R.layout.fragment_post;
    }

    @Override
    protected boolean hasHomeBackEnabled() {
        return false;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initCompositeDisposable();
        initRecyclerView();

        setTitle(postScreenTitle);
        compositeDisposable.add(adapter.getModelSubject()
            .subscribe(postModel -> {
                navigator.openPostDetailsFragment(R.id.container, postModel.id(), PostDetailsFragment.TAG);
            }, this::showError)
        );
    }

    @Override
    public void loadPostList(@NonNull List<PostModel> postModelList) {
        adapter.setPostModelList(postModelList);
        adapter.notifyDataSetChanged();
        getViewState().setPostModelList(postModelList);
    }

    @Override
    public void showError(@NonNull Throwable error) {
        Timber.e(error, "we had an error");
        Toast.makeText(getContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showLoadingBar() {
        progressBarView.setVisibility(View.VISIBLE);
        recyclerView.setVisibility(View.GONE);
    }

    @Override
    public void hideLoadingBar() {
        progressBarView.setVisibility(View.GONE);
        recyclerView.setVisibility(View.VISIBLE);
    }

    private void initRecyclerView() {
        final LayoutManager lm = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(lm);
        adapter = new PostAdapter(compositeDisposable);
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (compositeDisposable != null && !compositeDisposable.isDisposed()) {
            compositeDisposable.dispose();
            compositeDisposable = null;
        }
    }

    private void initCompositeDisposable() {
        if (compositeDisposable == null) {
            compositeDisposable = new CompositeDisposable();
        }
    }
}
