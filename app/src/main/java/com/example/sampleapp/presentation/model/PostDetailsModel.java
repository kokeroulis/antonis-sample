package com.example.sampleapp.presentation.model;

import android.support.annotation.NonNull;

import com.google.auto.value.AutoValue;

@AutoValue
public abstract class PostDetailsModel {

    public abstract UserModel userModel();
    public abstract PostModel postModel();
    public abstract int commentCount();

    @NonNull
    public static PostDetailsModel.Builder builder() {
        return new AutoValue_PostDetailsModel.Builder();
    }

    @AutoValue.Builder
    public interface Builder {
        Builder userModel(UserModel userModel);
        Builder postModel(PostModel postModel);
        Builder commentCount(int commentCount);
        PostDetailsModel build();
    }
}
