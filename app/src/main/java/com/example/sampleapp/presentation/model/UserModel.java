package com.example.sampleapp.presentation.model;

import android.support.annotation.NonNull;

import com.google.auto.value.AutoValue;

@AutoValue
public abstract class UserModel {
    public abstract int id();
    public abstract String userName();
    public abstract String email();

    @NonNull
    public static UserModel.Builder builder() {
        return new AutoValue_UserModel.Builder();
    }

    @AutoValue.Builder
    public interface Builder {
        Builder id(int id);
        Builder userName(String userName);
        Builder email(String email);
        UserModel build();
    }

}
