package com.example.sampleapp.presentation.post;

import dagger.Module;
import dagger.Provides;
import com.example.sampleapp.dagger.scopes.FragmentScope;
import com.example.sampleapp.domain.GetPostInteractor;
import com.example.sampleapp.presentation.model.PostModelMapper;
import com.example.sampleapp.presentation.post.mvp.PostPresenter;

@Module
public class PostModule {

    @Provides
    @FragmentScope
    PostPresenter providesPostPresenter(GetPostInteractor postInteractor, PostModelMapper postModelMapper) {
        return new PostPresenter(postInteractor, postModelMapper);
    }
}
