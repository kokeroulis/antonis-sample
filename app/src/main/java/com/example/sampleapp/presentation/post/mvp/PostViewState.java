package com.example.sampleapp.presentation.post.mvp;

import android.support.annotation.Nullable;

import com.hannesdorfmann.mosby3.mvp.viewstate.ViewState;

import java.util.List;

import com.example.sampleapp.presentation.model.PostModel;
import timber.log.Timber;

public class PostViewState implements ViewState<PostView> {

    @Nullable
    private List<PostModel> postModelList;

    @Override
    public void apply(PostView view, boolean retained) {
        if (postModelList != null) {
            Timber.d("The posts has been restored from the viewstate");
            view.hideLoadingBar();
            view.loadPostList(postModelList);
        }
    }

    public void setPostModelList(@Nullable List<PostModel> postModelList) {
        this.postModelList = postModelList;
    }
}
