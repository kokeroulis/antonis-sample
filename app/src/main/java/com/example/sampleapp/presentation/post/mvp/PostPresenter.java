package com.example.sampleapp.presentation.post.mvp;

import android.support.annotation.Nullable;

import com.hannesdorfmann.mosby3.mvp.MvpBasePresenter;

import com.example.sampleapp.domain.GetPostInteractor;
import com.example.sampleapp.presentation.model.PostModelMapper;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import polanski.option.Option;

public class PostPresenter extends MvpBasePresenter<PostView> {

    @Nullable Disposable subscription;
    private final GetPostInteractor postInteractor;
    private final PostModelMapper postModelMapper;

    public PostPresenter(GetPostInteractor postInteractor, PostModelMapper postModelMapper) {
        this.postInteractor = postInteractor;
        this.postModelMapper = postModelMapper;
    }

    public void loadPosts() {
        subscription = postInteractor.getResult(Option.none())
            .map(postModelMapper::transform)
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe(subscription1 -> {
                if (getView() != null) getView().showLoadingBar();
            })
            .subscribe(postList -> {
                if (getView() != null) {
                    getView().hideLoadingBar();
                    getView().loadPostList(postList);
                }
            }, error -> {
                if (getView() != null) {
                    getView().hideLoadingBar();
                    getView().showError(error);
                }
            });
    }

    @Override
    public void detachView(boolean retainInstance) {
        super.detachView(retainInstance);
        if (!retainInstance && subscription != null && !subscription.isDisposed()) {
            subscription.dispose();
            subscription = null;
        }
    }
}
