package com.example.sampleapp.presentation.postDetails.mvp;

import android.support.annotation.NonNull;

import com.hannesdorfmann.mosby3.mvp.MvpView;

import com.example.sampleapp.presentation.model.PostDetailsModel;

public interface PostDetailsView extends MvpView {

    void loadPostDetails(@NonNull PostDetailsModel postDetailsModel);

    void showError(@NonNull Throwable error);

    void showLoadingBar();

    void hideLoadingBar();
}
