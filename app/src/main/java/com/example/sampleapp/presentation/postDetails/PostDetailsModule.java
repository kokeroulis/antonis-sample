package com.example.sampleapp.presentation.postDetails;

import dagger.Module;
import dagger.Provides;
import com.example.sampleapp.dagger.scopes.FragmentScope;
import com.example.sampleapp.domain.GetPostDetailsInteractor;
import com.example.sampleapp.presentation.model.PostDetailsMapper;
import com.example.sampleapp.presentation.postDetails.mvp.PostDetailsPresenter;

@Module
public class PostDetailsModule {

    @Provides
    @FragmentScope
    PostDetailsPresenter providesPostPresenter(GetPostDetailsInteractor postDetailsInteractor,
                                               PostDetailsMapper postDetailsMapper) {
        return new PostDetailsPresenter(postDetailsInteractor, postDetailsMapper);
    }
}
