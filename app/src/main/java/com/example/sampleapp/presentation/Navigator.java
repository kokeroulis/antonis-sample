package com.example.sampleapp.presentation;

import android.support.annotation.IdRes;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentManager;

import javax.inject.Inject;

import com.example.sampleapp.presentation.post.PostFragment;
import com.example.sampleapp.presentation.postDetails.PostDetailsFragment;

public class Navigator {

    @NonNull
    private final FragmentManager supportFragmentManager;

    @Inject
    public Navigator(@NonNull FragmentManager supportFragmentManager) {
        this.supportFragmentManager = supportFragmentManager;
    }

    public void openPostFragment(@IdRes int containerId, @NonNull String tag) {
        supportFragmentManager.beginTransaction()
            .add(containerId, new PostFragment(), tag)
            .commit();
    }

    public void openPostDetailsFragment(@IdRes int containerId, int postId, @NonNull String tag) {
        supportFragmentManager.beginTransaction()
            .replace(containerId, PostDetailsFragment.newInstance(postId), tag)
            .addToBackStack(tag)
            .commit();
    }
}
