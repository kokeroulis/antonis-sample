package com.example.sampleapp.presentation.model;

import android.support.annotation.NonNull;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.inject.Inject;

import com.example.sampleapp.domain.models.Post;
import com.example.sampleapp.domain.models.PostWithUserAvatar;

public class PostModelMapper {

    @Inject
    public PostModelMapper() {}

    @NonNull
    public List<PostModel> transform(@NonNull List<PostWithUserAvatar> postList) {
        final List<PostModel> postModelList = new ArrayList<>();

        for (PostWithUserAvatar postWithAvatar : postList) {
            postModelList.add(
                transform(postWithAvatar)
            );
        }

        return Collections.unmodifiableList(postModelList);
    }


    @NonNull
    public PostModel transform(@NonNull PostWithUserAvatar postWithAvatar) {
        return PostModel.builder()
            .id(postWithAvatar.post().id())
            .userId(postWithAvatar.post().userId())
            .title(postWithAvatar.post().title())
            .body(postWithAvatar.post().body())
            .avatarUrl(postWithAvatar.avatarUrl())
            .build();
    }

    @NonNull
    public PostModel transformFromPost(@NonNull Post post, @NonNull String userEmail) {
        return PostModel.builder()
            .id(post.id())
            .userId(post.userId())
            .title(post.title())
            .body(post.body())
            .avatarUrl(PostWithUserAvatar.createUserAvatar(userEmail))
            .build();
    }
}
