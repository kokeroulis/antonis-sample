package com.example.sampleapp.presentation.model;

import android.support.annotation.NonNull;

import javax.inject.Inject;

import com.example.sampleapp.domain.models.PostDetails;

public class PostDetailsMapper {
    @NonNull private final UserModelMapper userModelMapper;
    @NonNull private final PostModelMapper postModelMapper;

    @Inject
    public PostDetailsMapper(@NonNull UserModelMapper userModelMapper,
                             @NonNull PostModelMapper postModelMapper) {
        this.userModelMapper = userModelMapper;
        this.postModelMapper = postModelMapper;
    }

    @NonNull
    public PostDetailsModel transform(@NonNull PostDetails postDetails) {
        final UserModel userModel = userModelMapper.transform(postDetails.user());
        final PostModel postModel = postModelMapper.transformFromPost(postDetails.post(), userModel.email());

        return PostDetailsModel.builder()
            .postModel(postModel)
            .userModel(userModel)
            .commentCount(postDetails.commentCount())
            .build();
    }
}
