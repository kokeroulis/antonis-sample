package com.example.sampleapp.presentation.model;

import android.support.annotation.NonNull;

import javax.inject.Inject;

import com.example.sampleapp.domain.models.User;

public class UserModelMapper {

    @Inject
    public UserModelMapper() {}

    @NonNull
    public UserModel transform(@NonNull User user) {
        return UserModel.builder()
            .id(user.id())
            .email(user.email())
            .userName(user.userName())
            .build();
    }
}
