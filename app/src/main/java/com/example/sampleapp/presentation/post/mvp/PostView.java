package com.example.sampleapp.presentation.post.mvp;

import android.support.annotation.NonNull;

import com.hannesdorfmann.mosby3.mvp.MvpView;

import java.util.List;

import com.example.sampleapp.presentation.model.PostModel;

public interface PostView extends MvpView {

    void loadPostList(@NonNull List<PostModel> postModelList);

    void showError(@NonNull Throwable error);

    void showLoadingBar();

    void hideLoadingBar();
}
