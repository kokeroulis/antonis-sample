package com.example.sampleapp.presentation.postDetails;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.CardView;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import butterknife.BindString;
import butterknife.BindView;
import com.example.sampleapp.R;
import com.example.sampleapp.base.BaseMvpFragment;
import com.example.sampleapp.presentation.model.PostDetailsModel;
import com.example.sampleapp.presentation.postDetails.mvp.PostDetailsPresenter;
import com.example.sampleapp.presentation.postDetails.mvp.PostDetailsView;
import com.example.sampleapp.presentation.postDetails.mvp.PostDetailsViewState;
import timber.log.Timber;

public class PostDetailsFragment extends BaseMvpFragment<PostDetailsView,
    PostDetailsPresenter, PostDetailsViewState>
    implements PostDetailsView {

    public static final String POST_ID = "post_id";
    @BindView(R.id.progressBar) ProgressBar progressBarView;
    @BindView(R.id.contentView) CardView contextView;
    @BindView(R.id.avatar) ImageView avatarView;
    @BindView(R.id.title) TextView titleView;
    @BindView(R.id.body) TextView bodyView;
    @BindView(R.id.userName) TextView userNameView;
    @BindView(R.id.commentCount) TextView commentCountView;
    @BindString(R.string.post_screen_title_detail) String postScreenDetailTitle;

    public static final String TAG = PostDetailsFragment.class.getSimpleName();

    @NonNull
    public static PostDetailsFragment newInstance(final int postId) {
        final PostDetailsFragment fragment = new PostDetailsFragment();
        final Bundle args = new Bundle();
        args.putInt(POST_ID, postId);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onNewViewStateInstance() {
        super.onNewViewStateInstance();
        presenter.loadPostDetails(getPostId());
    }

    @NonNull
    @Override
    public PostDetailsViewState createViewState() {
        return new PostDetailsViewState();
    }

    @Override
    protected int getLayoutRes() {
        return R.layout.fragment_post_details;
    }

    @Override
    protected boolean hasHomeBackEnabled() {
        return true;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setTitle(postScreenDetailTitle);
    }

    @Override
    public void loadPostDetails(@NonNull PostDetailsModel postDetailsModel) {
        titleView.setText(postDetailsModel.postModel().title());
        bodyView.setText(postDetailsModel.postModel().body());
        userNameView.setText(postDetailsModel.userModel().userName());
        commentCountView.setText(String.valueOf(postDetailsModel.commentCount()));

        Glide.with(this)
            .load(postDetailsModel.postModel().avatarUrl())
            .into(avatarView);
        getViewState().setPostDetailsModel(postDetailsModel);
    }

    @Override
    public void showError(@NonNull Throwable error) {
        Timber.e(error, "we had an error");
        Toast.makeText(getContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showLoadingBar() {
        progressBarView.setVisibility(View.VISIBLE);
        contextView.setVisibility(View.GONE);
    }

    @Override
    public void hideLoadingBar() {
        progressBarView.setVisibility(View.GONE);
        contextView.setVisibility(View.VISIBLE);
    }

    private int getPostId() {
        return getArguments().getInt(POST_ID);
    }
}
