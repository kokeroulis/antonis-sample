package com.example.sampleapp.presentation.postDetails.mvp;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.hannesdorfmann.mosby3.mvp.MvpBasePresenter;

import com.example.sampleapp.domain.GetPostDetailsInteractor;
import com.example.sampleapp.presentation.model.PostDetailsMapper;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import polanski.option.Option;

public class PostDetailsPresenter extends MvpBasePresenter<PostDetailsView> {

    @Nullable Disposable subscription;
    @NonNull
    private final GetPostDetailsInteractor postDetailsInteractor;
    @NonNull
    private final PostDetailsMapper postDetailsMapper;

    public PostDetailsPresenter(@NonNull GetPostDetailsInteractor postDetailsInteractor,
                                @NonNull PostDetailsMapper postDetailsMapper) {
        this.postDetailsInteractor = postDetailsInteractor;
        this.postDetailsMapper = postDetailsMapper;
    }

    public void loadPostDetails(int postId) {
        subscription = postDetailsInteractor.getResult(Option.ofObj(postId))
            .map(postDetailsMapper::transform)
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe(subscription1 -> {
                if (getView() != null) getView().showLoadingBar();
            })
            .subscribe(postDetails -> {
                if (getView() != null) {
                    getView().hideLoadingBar();
                    getView().loadPostDetails(postDetails);
                }
            }, error -> {
                if (getView() != null) {
                    getView().hideLoadingBar();
                    getView().showError(error);
                }
            });
    }

    @Override
    public void detachView(boolean retainInstance) {
        super.detachView(retainInstance);
        if (!retainInstance && subscription != null && !subscription.isDisposed()) {
            subscription.dispose();
            subscription = null;
        }
    }
}
