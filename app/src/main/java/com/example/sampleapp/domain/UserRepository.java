package com.example.sampleapp.domain;

import android.support.annotation.NonNull;

import java.util.List;

import com.example.sampleapp.domain.models.User;
import io.reactivex.Completable;
import io.reactivex.Flowable;
import polanski.option.Option;

public interface UserRepository {

    @NonNull
    Flowable<List<User>> getUsers();

    @NonNull
    Flowable<Option<User>> getUserById(int userId);

    @NonNull
    Completable forceUpdate();
}
