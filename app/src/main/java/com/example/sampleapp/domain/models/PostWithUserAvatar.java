package com.example.sampleapp.domain.models;

import android.support.annotation.NonNull;

import com.google.auto.value.AutoValue;

@AutoValue
public abstract class PostWithUserAvatar {
    public static final String AVATAR_END_POINT = "https://api.adorable.io/avatars/285/";

    public abstract Post post();
    public abstract String avatarUrl();

    public static PostWithUserAvatar create(@NonNull Post post, @NonNull String userEmail) {
        return new AutoValue_PostWithUserAvatar.Builder()
            .post(post)
            .avatarUrl(createUserAvatar(userEmail))
            .build();
    }

    @NonNull
    public static String createUserAvatar(@NonNull String userEmail) {
        return AVATAR_END_POINT + userEmail;
    }

    @AutoValue.Builder
    public interface Builder {
        Builder post(Post post);
        Builder avatarUrl(String avatarUrl);
        PostWithUserAvatar build();
    }
}
