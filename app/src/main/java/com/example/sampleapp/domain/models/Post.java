package com.example.sampleapp.domain.models;

import android.support.annotation.NonNull;

import com.google.auto.value.AutoValue;

@AutoValue
public abstract class Post {
    public abstract int id();
    public abstract int userId();
    public abstract String title();
    public abstract String body();

    @NonNull
    public static Post.Builder builder() {
        return new AutoValue_Post.Builder();
    }

    @AutoValue.Builder
    public interface Builder {
        Builder id(int id);
        Builder userId(int userId);
        Builder title(String title);
        Builder body(String body);
        Post build();
    }
}
