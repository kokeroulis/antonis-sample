package com.example.sampleapp.domain;

import android.support.annotation.NonNull;

import java.util.List;

import com.example.sampleapp.domain.models.Post;
import io.reactivex.Completable;
import io.reactivex.Flowable;

public interface PostRepository {

    @NonNull
    Flowable<List<Post>> getPosts();

    @NonNull
    Flowable<Post> findPostById(int postId);

    @NonNull
    Completable forceUpdate();
}
