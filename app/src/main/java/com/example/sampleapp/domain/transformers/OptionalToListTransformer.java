package com.example.sampleapp.domain.transformers;

import org.reactivestreams.Publisher;

import java.util.List;

import io.reactivex.Flowable;
import io.reactivex.FlowableTransformer;
import polanski.option.Option;
import polanski.option.OptionUnsafe;

public class OptionalToListTransformer<T> implements FlowableTransformer<Option<List<T>>, List<T>> {

    @Override
    public Publisher<List<T>> apply(@io.reactivex.annotations.NonNull Flowable<Option<List<T>>> upstream) {
        return upstream.filter(Option::isSome)
            .map(OptionUnsafe::getUnsafe);
    }
}
