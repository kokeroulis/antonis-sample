package com.example.sampleapp.domain;

import android.support.annotation.NonNull;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import javax.inject.Inject;

import com.example.sampleapp.domain.models.Post;
import com.example.sampleapp.domain.models.PostWithUserAvatar;
import com.example.sampleapp.domain.models.User;
import com.example.sampleapp.domain.transformers.ListToOptionalTransformer;
import com.example.sampleapp.domain.transformers.OptionalToListTransformer;
import io.reactivex.Completable;
import io.reactivex.Flowable;
import io.reactivex.Single;
import polanski.option.Option;

public class GetPostInteractor implements Interactor<Void, List<PostWithUserAvatar>> {

    @NonNull private final PostRepository repository;
    @NonNull private final UserRepository userRepository;

    @Inject
    public GetPostInteractor(@NonNull PostRepository repository, @NonNull UserRepository userRepository) {
        this.repository = repository;
        this.userRepository = userRepository;
    }


    @Override
    public Flowable<List<PostWithUserAvatar>> getResult(@NonNull final Option<Void> params) {
        // we need to fetch and store in the db, the users first for the foreign key
        return getUsers()
            .switchMap(userList -> {
                return getPosts()
                    .map(postList -> transformToPostWithAvatar(postList, userList));
            });
    }

    private List<PostWithUserAvatar> transformToPostWithAvatar(@NonNull List<Post> postList, @NonNull List<User> userList) {
        final List<PostWithUserAvatar> postWithUserAvatarList = new ArrayList<>();
        final Map<Integer, String> userIdAndEmail = new ConcurrentHashMap<>();
        for (User user : userList) {
            userIdAndEmail.put(user.id(), user.email());
        }

        for (Post post : postList) {
            final String userEmail = userIdAndEmail.get(post.userId());
            final PostWithUserAvatar postWithUserAvatar = PostWithUserAvatar.create(post, userEmail);
            postWithUserAvatarList.add(postWithUserAvatar);
        }

        return Collections.unmodifiableList(postWithUserAvatarList);
    }

    private Flowable<List<User>> getUsers() {
        return userRepository.getUsers()
            .compose(new ListToOptionalTransformer<>())
            .flatMapSingle(this::fetchUserList)
            .compose(new OptionalToListTransformer<>())
            .filter(users -> users.size() > 0);
    }

    private Single<Option<List<User>>> fetchUserList(final @NonNull Option<List<User>> user) {
        return fetchUserWhenEmpty(user).andThen(Single.just(user));
    }

    private Completable fetchUserWhenEmpty(@NonNull Option<List<User>> userList) {
        return userList.isNone() ? userRepository.forceUpdate() : Completable.complete();
    }

    private Flowable<List<Post>> getPosts() {
        return repository.getPosts()
            .compose(new ListToOptionalTransformer<>())
            .flatMapSingle(this::fetchPostList)
            .compose(new OptionalToListTransformer<>())
            .filter(postList -> postList.size() > 0);
    }

    private Single<Option<List<Post>>> fetchPostList(@NonNull Option<List<Post>> post) {
        return fetchPostWhenEmpty(post).andThen(Single.just(post));
    }

    private Completable fetchPostWhenEmpty(@NonNull Option<List<Post>> postList) {
        return postList.isNone() ? repository.forceUpdate() : Completable.complete();
    }

}
