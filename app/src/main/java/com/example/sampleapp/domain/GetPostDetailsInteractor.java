package com.example.sampleapp.domain;

import android.support.annotation.NonNull;

import java.util.List;

import javax.inject.Inject;

import com.example.sampleapp.domain.models.Comment;
import com.example.sampleapp.domain.models.PostDetails;
import com.example.sampleapp.domain.models.User;
import com.example.sampleapp.domain.transformers.ListToOptionalTransformer;
import io.reactivex.Completable;
import io.reactivex.Flowable;
import io.reactivex.Single;
import polanski.option.Option;
import polanski.option.OptionUnsafe;

public class GetPostDetailsInteractor implements Interactor<Integer, PostDetails> {

    @NonNull private final PostRepository postRepository;
    @NonNull private final UserRepository userRepository;
    @NonNull private final CommentRepository commentRepository;

    @Inject
    public GetPostDetailsInteractor(@NonNull PostRepository postRepository,
                                    @NonNull UserRepository userRepository,
                                    @NonNull CommentRepository commentRepository) {
        this.postRepository = postRepository;
        this.userRepository = userRepository;
        this.commentRepository = commentRepository;
    }


    @Override
    public Flowable<PostDetails> getResult(@NonNull Option<Integer> params) {
        final int postId = OptionUnsafe.getUnsafe(params);
        return postRepository.findPostById(postId)
            .switchMap(post -> {
                return Flowable.combineLatest(
                    getUser(post.userId()),
                    getCommentCount(post.id()),
                    (user, commentCount) -> PostDetails.create(post, user, commentCount));
            });
    }

    private Flowable<Integer> getCommentCount(int postId) {
        return commentRepository.getCommentByPostId(postId)
            .compose(new ListToOptionalTransformer<>())
            .flatMapSingle(this::fetchCommentList)
            .filter(Option::isSome)
            .map(OptionUnsafe::getUnsafe)
            .map(List::size);
    }

    private Flowable<User> getUser(final int userId) {
        return userRepository.getUserById(userId)
            .flatMapSingle(this::fetchUserList)
            .filter(Option::isSome)
            .map(OptionUnsafe::getUnsafe);
    }

    private Single<Option<User>> fetchUserList(@NonNull Option<User> userList) {
        return fetchUserListWhenEmpty(userList).andThen(Single.just(userList));
    }

    private Completable fetchUserListWhenEmpty(final @NonNull Option<User> userList) {
        return userList.isNone() ? userRepository.forceUpdate() : Completable.complete();
    }


    private Single<Option<List<Comment>>> fetchCommentList(@NonNull Option<List<Comment>> commentList) {
        return fetchCommentListWhenEmpty(commentList).andThen(Single.just(commentList));
    }

    private Completable fetchCommentListWhenEmpty(final @NonNull Option<List<Comment>> commentList) {
        return commentList.isNone() ? commentRepository.forceUpdate() : Completable.complete();
    }
}
