package com.example.sampleapp.domain.models;

import android.support.annotation.NonNull;

import com.google.auto.value.AutoValue;

@AutoValue
public abstract class PostDetails {
    public abstract Post post();
    public abstract User user();
    public abstract int commentCount();


    public static PostDetails create(@NonNull Post post, @NonNull User user, int commentCount) {
        return new AutoValue_PostDetails.Builder()
            .post(post)
            .user(user)
            .commentCount(commentCount)
            .build();
    }

    @AutoValue.Builder
    public interface Builder {
        Builder post(Post post);
        Builder user(User user);
        Builder commentCount(int commentCount);
        PostDetails build();
    }
}
