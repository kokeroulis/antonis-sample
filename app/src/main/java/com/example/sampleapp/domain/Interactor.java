package com.example.sampleapp.domain;

import android.support.annotation.NonNull;

import io.reactivex.Flowable;
import polanski.option.Option;

public interface Interactor<Params, T> {

    Flowable<T> getResult(@NonNull final Option<Params> params);
}
