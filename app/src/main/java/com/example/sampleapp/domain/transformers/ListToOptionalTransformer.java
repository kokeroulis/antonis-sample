package com.example.sampleapp.domain.transformers;

import org.reactivestreams.Publisher;

import java.util.List;

import io.reactivex.Flowable;
import io.reactivex.FlowableTransformer;
import polanski.option.Option;

public class ListToOptionalTransformer<T> implements FlowableTransformer<List<T>, Option<List<T>>> {

    @Override
    public Publisher<Option<List<T>>> apply(@io.reactivex.annotations.NonNull Flowable<List<T>> upstream) {
        return upstream
            .map(list -> {
                return list.size() == 0 ? Option.<List<T>>none() : Option.ofObj(list);
            });
    }
}
