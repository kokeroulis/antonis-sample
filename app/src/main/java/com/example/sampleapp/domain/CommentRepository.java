package com.example.sampleapp.domain;

import android.support.annotation.NonNull;

import java.util.List;

import com.example.sampleapp.domain.models.Comment;
import io.reactivex.Completable;
import io.reactivex.Flowable;

public interface CommentRepository {

    @NonNull
    Flowable<List<Comment>> getCommentByPostId(int postId);

    @NonNull
    Completable forceUpdate();
}
