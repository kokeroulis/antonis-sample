package com.example.sampleapp.domain.models;

import android.support.annotation.NonNull;

import com.google.auto.value.AutoValue;


@AutoValue
public abstract class User {
    public abstract int id();
    public abstract String userName();
    public abstract String email();

    @NonNull
    public static User.Builder builder() {
        return new AutoValue_User.Builder();
    }

    @AutoValue.Builder
    public interface Builder {
        Builder id(int id);
        Builder userName(String userName);
        Builder email(String email);
        User build();
    }
}


