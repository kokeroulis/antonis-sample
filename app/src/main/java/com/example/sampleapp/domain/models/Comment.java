package com.example.sampleapp.domain.models;

import android.support.annotation.NonNull;

import com.google.auto.value.AutoValue;

@AutoValue
public abstract class Comment {
    public abstract int id();
    public abstract int postId();

    @NonNull
    public static Comment.Builder builder() {
        return new AutoValue_Comment.Builder();
    }

    @AutoValue.Builder
    public interface Builder {
        Builder id(int id);
        Builder postId(int postId);
        Comment build();
    }
}
