package com.example.sampleapp.base;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.hannesdorfmann.mosby3.mvp.MvpPresenter;
import com.hannesdorfmann.mosby3.mvp.MvpView;
import com.hannesdorfmann.mosby3.mvp.viewstate.MvpViewStateFragment;
import com.hannesdorfmann.mosby3.mvp.viewstate.ViewState;

import javax.inject.Inject;
import javax.inject.Provider;

import butterknife.ButterKnife;
import butterknife.Unbinder;
import dagger.android.support.AndroidSupportInjection;

public abstract class BaseMvpFragment<VIEW extends MvpView,
    PRESENTER extends MvpPresenter<VIEW>,
    VS extends ViewState<VIEW>> extends MvpViewStateFragment<VIEW, PRESENTER, VS> {

    @Inject Provider<PRESENTER> presenterProvider;
    @Nullable
    private Unbinder unbinder;

    @Override
    public PRESENTER createPresenter() {
        return presenterProvider.get();
    }

    @NonNull
    @Override
    public abstract VS createViewState();

    @Override
    public void onNewViewStateInstance() {
        // overide this if needed
    }

    protected void setTitle(@NonNull String title) {
        final ActionBar actionBar = getActionBar();
        if (actionBar != null) {
            actionBar.setTitle(title);
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(getLayoutRes(), container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initializeHomeBack();
        unbinder = ButterKnife.bind(this, view);
    }

    protected abstract int getLayoutRes();

    protected abstract boolean hasHomeBackEnabled();

    protected void initializeHomeBack() {
        final ActionBar actionBar = getActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(hasHomeBackEnabled());
        }
    }

    @Nullable
    protected ActionBar getActionBar() {
        if (getActivity() instanceof AppCompatActivity) {
            return ((AppCompatActivity) getActivity()).getSupportActionBar();
        } else {
            return null;
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        AndroidSupportInjection.inject(this);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (unbinder != null) {
            unbinder.unbind();
            unbinder = null;
        }
    }
}
