package com.example.sampleapp.dagger.modules;

import android.app.Application;
import android.content.Context;
import android.support.annotation.NonNull;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import com.example.sampleapp.data.network.ReplaceBaseUrl;

@Module
public class ApplicationModule {
    @NonNull
    private final Application application;


    public ApplicationModule(@NonNull Application application) {
        this.application = application;
    }


    @Provides
    @Singleton
    Context providesApplicationContext() {
        return application.getApplicationContext();
    }


    @Provides
    @Singleton
    ReplaceBaseUrl providesReplaceBaseUrl() {
        return new ReplaceBaseUrl();
    }
}
