package com.example.sampleapp.dagger.components;

import javax.inject.Singleton;

import dagger.Component;
import com.example.sampleapp.MyApplication;
import com.example.sampleapp.dagger.modules.ActivityContributorModule;
import com.example.sampleapp.dagger.modules.ApplicationModule;
import com.example.sampleapp.dagger.modules.DatabaseModule;
import com.example.sampleapp.dagger.modules.NetworkModule;
import com.example.sampleapp.data.network.ReplaceBaseUrl;
import com.example.sampleapp.data.network.RestService;
import com.example.sampleapp.data.repository.comment.CommentDataModule;
import com.example.sampleapp.data.repository.post.PostDataModule;
import com.example.sampleapp.data.repository.user.UserDataModule;

@Singleton
@Component(modules = {
    ApplicationModule.class, NetworkModule.class,
    DatabaseModule.class, ActivityContributorModule.class,
    PostDataModule.class, UserDataModule.class,
    CommentDataModule.class
})
public interface ApplicationComponent {

    void inject(MyApplication application);

    RestService restService();
    ReplaceBaseUrl replaceBaseUrl();
}
