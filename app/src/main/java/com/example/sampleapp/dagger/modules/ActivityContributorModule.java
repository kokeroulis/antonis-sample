package com.example.sampleapp.dagger.modules;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;
import com.example.sampleapp.MainActivity;
import com.example.sampleapp.MainActivityModule;
import com.example.sampleapp.dagger.scopes.ActivityScope;

@Module
public abstract class ActivityContributorModule {

    @ActivityScope
    @ContributesAndroidInjector(modules = MainActivityModule.class)
    abstract MainActivity contributesMainActivityInjector();
}
