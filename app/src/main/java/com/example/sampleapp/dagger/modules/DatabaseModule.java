package com.example.sampleapp.dagger.modules;

import android.arch.persistence.room.Room;
import android.content.Context;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import com.example.sampleapp.data.database.ApplicationDatabase;

@Module
public class DatabaseModule {

    @Provides
    @Singleton
    ApplicationDatabase providesApplicationDatabase(@Singleton Context context) {
        return Room.databaseBuilder(context, ApplicationDatabase.class, "application_db").build();
    }
}
