package com.example.sampleapp.dagger.modules;

import com.facebook.stetho.okhttp3.StethoInterceptor;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import com.example.sampleapp.BuildConfig;
import com.example.sampleapp.data.network.CommentResponseAdapterFactory;
import com.example.sampleapp.data.network.PostResponseAdapterFactory;
import com.example.sampleapp.data.network.ReplaceBaseUrl;
import com.example.sampleapp.data.network.RestService;
import com.example.sampleapp.data.network.UserResponseAdapterFactory;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import okhttp3.logging.HttpLoggingInterceptor.Level;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

@Module
public class NetworkModule {

    @Provides
    @Singleton
    HttpLoggingInterceptor providesHttpLogginInterceptor() {
        final HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();
        loggingInterceptor.setLevel(Level.BODY);
        return loggingInterceptor;
    }

    @Provides
    @Singleton
    OkHttpClient providesOkHttpClient(HttpLoggingInterceptor loggingInterceptor) {
        OkHttpClient.Builder builder = new OkHttpClient.Builder();

        if (BuildConfig.DEBUG) {
            builder.addInterceptor(loggingInterceptor);
            builder.addNetworkInterceptor(new StethoInterceptor());
        }
        return builder.build();
    }

    @Provides
    @Singleton
    Gson providesGson() {
        final GsonBuilder builder = new GsonBuilder();

        builder.registerTypeAdapterFactory(PostResponseAdapterFactory.create());
        builder.registerTypeAdapterFactory(UserResponseAdapterFactory.create());
        builder.registerTypeAdapterFactory(CommentResponseAdapterFactory.create());
        return builder.create();
    }

    @Provides
    @Singleton
    Retrofit providesRetrofit(Gson gson, OkHttpClient client, ReplaceBaseUrl baseUrl) {
        return new Retrofit.Builder().addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .addConverterFactory(GsonConverterFactory.create(gson))
            .client(client)
            .baseUrl(baseUrl.getBaseUrl().get())
            .build();
    }

    @Provides
    @Singleton
    RestService providesRestService(Retrofit retrofit) {
        return retrofit.create(RestService.class);
    }
}
