package com.example.sampleapp.data.repository.user;

import android.support.annotation.NonNull;

import java.util.List;

import com.example.sampleapp.data.entity.UserEntity;
import com.example.sampleapp.data.entity.mapper.UserResponseMapper;
import com.example.sampleapp.data.network.RestService;
import io.reactivex.Completable;
import io.reactivex.Flowable;
import io.reactivex.Observable;
import io.reactivex.schedulers.Schedulers;
import polanski.option.Option;

public class RemoteUserDataStore implements UserDataStore {

    @NonNull private final RestService restService;
    @NonNull private final UserResponseMapper userResponseMapper;

    public RemoteUserDataStore(@NonNull RestService restService, @NonNull UserResponseMapper userResponseMapper) {
        this.restService = restService;
        this.userResponseMapper = userResponseMapper;
    }

    @Override
    public Flowable<List<UserEntity>> getUsers() {
        return restService.getUserList()
            .subscribeOn(Schedulers.io())
            .flatMapObservable(Observable::fromIterable)
            .map(userResponseMapper::transform)
            .toList()
            .toFlowable();
    }

    @Override
    public Flowable<Option<UserEntity>> getUserById(int userId) {
        throw new IllegalArgumentException("Find user by id is not supported");
    }

    @Override
    public Completable saveUsers(List<UserEntity> userList) {
        throw new IllegalArgumentException("Post request are not supported");
    }
}
