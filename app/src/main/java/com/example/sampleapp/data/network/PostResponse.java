package com.example.sampleapp.data.network;

import android.support.annotation.NonNull;

import com.google.auto.value.AutoValue;
import com.google.gson.Gson;
import com.google.gson.TypeAdapter;

@AutoValue
public abstract class PostResponse {
    public abstract int id();
    public abstract int userId();
    public abstract String title();
    public abstract String body();

    @NonNull
    public static PostResponse.Builder builder() {
        return new AutoValue_PostResponse.Builder();
    }

    @NonNull
    public static TypeAdapter<PostResponse> typeAdapter(@NonNull final Gson gson) {
        return new AutoValue_PostResponse.GsonTypeAdapter(gson);
    }

    @AutoValue.Builder
    public interface Builder {
        Builder id(int id);
        Builder userId(int userId);
        Builder title(String title);
        Builder body(String body);
        PostResponse build();
    }
}
