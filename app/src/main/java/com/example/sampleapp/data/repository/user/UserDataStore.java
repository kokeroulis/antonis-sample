package com.example.sampleapp.data.repository.user;

import java.util.List;

import com.example.sampleapp.data.entity.UserEntity;
import io.reactivex.Completable;
import io.reactivex.Flowable;
import polanski.option.Option;

public interface UserDataStore {

    Flowable<List<UserEntity>> getUsers();

    Flowable<Option<UserEntity>> getUserById(int userId);

    Completable saveUsers(List<UserEntity> userList);
}
