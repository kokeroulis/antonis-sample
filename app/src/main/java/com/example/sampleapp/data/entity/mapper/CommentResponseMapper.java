package com.example.sampleapp.data.entity.mapper;

import android.support.annotation.NonNull;

import javax.inject.Inject;

import com.example.sampleapp.data.entity.CommentEntity;
import com.example.sampleapp.data.network.CommentResponse;

public class CommentResponseMapper {

    @Inject
    public CommentResponseMapper() {}

    @NonNull
    public CommentEntity transform(@NonNull CommentResponse commentResponse) {
        final CommentEntity entity = new CommentEntity();
        entity.setId(commentResponse.id());
        entity.setPostId(commentResponse.postId());
        entity.setEmail(commentResponse.email());
        entity.setBody(commentResponse.body());
        entity.setName(commentResponse.name());
        return entity;
    }
}
