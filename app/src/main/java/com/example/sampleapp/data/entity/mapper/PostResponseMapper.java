package com.example.sampleapp.data.entity.mapper;

import android.support.annotation.NonNull;

import javax.inject.Inject;

import com.example.sampleapp.data.entity.PostEntity;
import com.example.sampleapp.data.network.PostResponse;

public class PostResponseMapper {

    @Inject
    public PostResponseMapper() {}

    @NonNull
    public PostEntity transform(@NonNull PostResponse postResponse) {
        final PostEntity entity = new PostEntity();
        entity.setId(postResponse.id());
        entity.setUserId(postResponse.userId());
        entity.setTitle(postResponse.title());
        entity.setBody(postResponse.body());
        return entity;
    }
}
