package com.example.sampleapp.data.network;

import java.util.List;

import io.reactivex.Single;
import retrofit2.http.GET;

public interface RestService {

    @GET("posts")
    Single<List<PostResponse>> getPostList();

    @GET("users")
    Single<List<UserResponse>> getUserList();

    @GET("comments")
    Single<List<CommentResponse>> getCommentList();
}
