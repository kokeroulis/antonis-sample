package com.example.sampleapp.data.database;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import java.util.List;

import com.example.sampleapp.data.entity.CommentEntity;
import io.reactivex.Flowable;

@Dao
public interface CommentDao {

    @Query("Select * from CommentEntity where postId = :postId")
    Flowable<List<CommentEntity>> getCommentByPostId(int postId);

    @Query("Select * from CommentEntity")
    Flowable<List<CommentEntity>> getAll();

    // we could also use this one in order to get the count of comments
    // we will leave this one here as an example
    @Query("Select count(*) from CommentEntity where postID = :postId")
    Flowable<Integer> getCommentCountByPostId(int postId);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAll(List<CommentEntity> userList);
}
