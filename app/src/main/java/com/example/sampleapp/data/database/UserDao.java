package com.example.sampleapp.data.database;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import java.util.List;

import com.example.sampleapp.data.entity.UserEntity;
import io.reactivex.Flowable;

@Dao
public interface UserDao {
    @Query("Select * from UserEntity")
    Flowable<List<UserEntity>> getAll();

    @Query("Select * from UserEntity where id = :userId")
    Flowable<List<UserEntity>> findUserById(int userId);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAll(List<UserEntity> userList);
}
