package com.example.sampleapp.data.database;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import java.util.List;

import com.example.sampleapp.data.entity.PostEntity;
import io.reactivex.Flowable;

@Dao
public interface PostDao {

    @Query("Select * from PostEntity")
    Flowable<List<PostEntity>> getAll();


    @Query("Select * from PostEntity where id = :postId")
    Flowable<List<PostEntity>> getPostById(int postId);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAll(List<PostEntity> postList);
}
