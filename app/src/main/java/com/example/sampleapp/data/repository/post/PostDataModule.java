package com.example.sampleapp.data.repository.post;

import javax.inject.Named;
import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import com.example.sampleapp.data.database.ApplicationDatabase;
import com.example.sampleapp.data.entity.mapper.PostMapper;
import com.example.sampleapp.data.entity.mapper.PostResponseMapper;
import com.example.sampleapp.data.network.RestService;
import com.example.sampleapp.domain.PostRepository;

@Module
public class PostDataModule {
    private static final String LOCAL_STORE = "local_store";
    private static final String REMOTE_STORE = "remote_store";

    @Provides
    @Singleton
    @Named(LOCAL_STORE)
    PostDataStore providesLocalDataStore(ApplicationDatabase applicationDatabase) {
        return new LocalPostDataStore(applicationDatabase.postDao());
    }

    @Provides
    @Singleton
    @Named(REMOTE_STORE)
    PostDataStore providesRemoteDataStore(RestService restService, PostResponseMapper responseMapper) {
        return new RemoteDataStore(restService, responseMapper);
    }


    @Provides
    @Singleton
    PostRepository providesPostRepository(@Named(LOCAL_STORE) PostDataStore localDataStore,
                                          @Named(REMOTE_STORE) PostDataStore remoteDataStore,
                                          PostMapper postMapper) {
        return new PostDataRepository(localDataStore, remoteDataStore, postMapper);
    }

}
