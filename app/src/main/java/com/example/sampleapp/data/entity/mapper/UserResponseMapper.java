package com.example.sampleapp.data.entity.mapper;

import android.support.annotation.NonNull;

import javax.inject.Inject;

import com.example.sampleapp.data.entity.UserEntity;
import com.example.sampleapp.data.network.UserResponse;

public class UserResponseMapper {

    @Inject
    public UserResponseMapper() {}

    @NonNull
    public UserEntity transform(@NonNull UserResponse userResponse) {
        final UserEntity entity = new UserEntity();
        entity.setId(userResponse.id());
        entity.setEmail(userResponse.email());
        entity.setUserName(userResponse.userName());
        return entity;
    }
}
