package com.example.sampleapp.data.repository.user;

import android.support.annotation.NonNull;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import com.example.sampleapp.data.entity.UserEntity;
import com.example.sampleapp.data.entity.mapper.UserMapper;
import com.example.sampleapp.domain.UserRepository;
import com.example.sampleapp.domain.models.User;
import io.reactivex.Completable;
import io.reactivex.Flowable;
import io.reactivex.schedulers.Schedulers;
import polanski.option.Option;
import polanski.option.OptionUnsafe;

@Singleton
public class UserDataRepository implements UserRepository {

    @NonNull
    private final UserDataStore userLocalDataStore;
    @NonNull
    private final UserDataStore userRemoteDataStore;
    @NonNull
    private final UserMapper userMapper;

    @Inject
    public UserDataRepository(@NonNull UserDataStore userLocalDataStore,
                              @NonNull UserDataStore userRemoteDataStore, @NonNull UserMapper userMapper) {
        this.userLocalDataStore = userLocalDataStore;
        this.userRemoteDataStore = userRemoteDataStore;
        this.userMapper = userMapper;
    }

    @NonNull
    @Override
    public Flowable<List<User>> getUsers() {
        return userLocalDataStore.getUsers()
            .subscribeOn(Schedulers.io())
            .map(userMapper::transform);
    }

    @NonNull
    @Override
    public Flowable<Option<User>> getUserById(int userId) {
        return userLocalDataStore.getUserById(userId)
            .subscribeOn(Schedulers.io())
            .map(userEntityOption -> {
                if (userEntityOption.isSome()) {
                    final UserEntity userEntity = OptionUnsafe.getUnsafe(userEntityOption);
                    final User user = userMapper.transform(userEntity);
                    return Option.ofObj(user);
                } else {
                    return Option.none();
                }
            });
    }

    @NonNull
    @Override
    public Completable forceUpdate() {
        return userRemoteDataStore.getUsers()
            .subscribeOn(Schedulers.io())
            .firstOrError()
            .flatMapCompletable(userLocalDataStore::saveUsers);
    }
}
