package com.example.sampleapp.data.repository.user;

import android.support.annotation.NonNull;

import java.util.List;

import com.example.sampleapp.data.database.UserDao;
import com.example.sampleapp.data.entity.UserEntity;
import io.reactivex.Completable;
import io.reactivex.Flowable;
import polanski.option.Option;

public class LocalUserDataStore implements UserDataStore {
    @NonNull
    private final UserDao userDao;

    public LocalUserDataStore(@NonNull UserDao userDao) {
        this.userDao = userDao;
    }

    @Override
    public Flowable<List<UserEntity>> getUsers() {
        return userDao.getAll();
    }

    @Override
    public Flowable<Option<UserEntity>> getUserById(int userId) {
        return userDao.findUserById(userId)
            .map(userList -> {
                return userList.size() == 0 ? Option.none() : Option.ofObj(userList.get(0));
            });
    }


    @Override
    public Completable saveUsers(List<UserEntity> userList) {
        return Completable.create(emitter -> {
            userDao.insertAll(userList);
            emitter.onComplete();
        });
    }
}
