package com.example.sampleapp.data.repository.comment;

import javax.inject.Named;
import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import com.example.sampleapp.data.database.ApplicationDatabase;
import com.example.sampleapp.data.entity.mapper.CommentMapper;
import com.example.sampleapp.data.entity.mapper.CommentResponseMapper;
import com.example.sampleapp.data.network.RestService;
import com.example.sampleapp.domain.CommentRepository;

@Module
public class CommentDataModule {
    private static final String LOCAL_STORE = "local_store";
    private static final String REMOTE_STORE = "remote_store";

    @Provides
    @Singleton
    @Named(LOCAL_STORE)
    CommentDataStore providesLocalDataStore(ApplicationDatabase applicationDatabase) {
        return new LocalCommentDataStore(applicationDatabase.commentDao());
    }

    @Provides
    @Singleton
    @Named(REMOTE_STORE)
    CommentDataStore providesRemoteDataStore(RestService restService, CommentResponseMapper responseMapper) {
        return new RemoteCommentDataStore(restService, responseMapper);
    }


    @Provides
    @Singleton
    CommentRepository providesPostRepository(@Named(LOCAL_STORE) CommentDataStore localDataStore,
                                             @Named(REMOTE_STORE) CommentDataStore remoteDataStore,
                                             CommentMapper commentMapper) {
        return new CommentDataRepository(localDataStore, remoteDataStore, commentMapper);
    }
}
