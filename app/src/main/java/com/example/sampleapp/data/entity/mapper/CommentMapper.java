package com.example.sampleapp.data.entity.mapper;

import android.support.annotation.NonNull;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.inject.Inject;

import com.example.sampleapp.data.entity.CommentEntity;
import com.example.sampleapp.domain.models.Comment;

public class CommentMapper {

    @Inject
    public CommentMapper() {}

    @NonNull
    public List<Comment> transform(@NonNull List<CommentEntity> commentEntityList) {
        final List<Comment> commentList = new ArrayList<>();

        for (CommentEntity entity : commentEntityList) {
            final Comment comment = Comment.builder()
                .id(entity.getId())
                .postId(entity.getPostId())
                .build();

            commentList.add(comment);
        }

        return Collections.unmodifiableList(commentList);
    }
}
