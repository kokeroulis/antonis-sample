package com.example.sampleapp.data.repository.comment;

import android.support.annotation.NonNull;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import com.example.sampleapp.data.entity.mapper.CommentMapper;
import com.example.sampleapp.domain.CommentRepository;
import com.example.sampleapp.domain.models.Comment;
import io.reactivex.Completable;
import io.reactivex.Flowable;
import io.reactivex.schedulers.Schedulers;

@Singleton
public class CommentDataRepository implements CommentRepository {

    @NonNull
    private final CommentDataStore localCommentDataStore;
    @NonNull
    private final CommentDataStore remoteCommentDataStore;
    @NonNull
    private final CommentMapper commentMapper;

    @Inject
    public CommentDataRepository(@NonNull CommentDataStore localCommentDataStore,
                                 @NonNull CommentDataStore remoteCommentDataStore,
                                 @NonNull CommentMapper commentMapper) {
        this.localCommentDataStore = localCommentDataStore;
        this.remoteCommentDataStore = remoteCommentDataStore;
        this.commentMapper = commentMapper;
    }

    @NonNull
    @Override
    public Flowable<List<Comment>> getCommentByPostId(int postId) {
        return localCommentDataStore.getCommentByPostId(postId)
            .subscribeOn(Schedulers.io())
            .map(commentMapper::transform);
    }

    @NonNull
    @Override
    public Completable forceUpdate() {
        return remoteCommentDataStore.getComments()
            .subscribeOn(Schedulers.io())
            .firstOrError()
            .flatMapCompletable(localCommentDataStore::saveComments);
    }
}
