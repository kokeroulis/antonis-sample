package com.example.sampleapp.data.network;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.google.auto.value.AutoValue;
import com.google.gson.Gson;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.SerializedName;

@AutoValue
public abstract class UserResponse {
    public abstract int id();
    @Nullable
    @SerializedName("username")
    public abstract String userName();
    public abstract String email();

    @NonNull
    public static UserResponse.Builder builder() {
        return new AutoValue_UserResponse.Builder();
    }

    @NonNull
    public static TypeAdapter<UserResponse> typeAdapter(@NonNull final Gson gson) {
        return new AutoValue_UserResponse.GsonTypeAdapter(gson);
    }

    @AutoValue.Builder
    public interface Builder {
        Builder id(int id);
        Builder userName(String userName);
        Builder email(String email);
        UserResponse build();
    }
}
