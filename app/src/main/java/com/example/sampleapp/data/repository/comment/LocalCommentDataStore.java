package com.example.sampleapp.data.repository.comment;

import android.support.annotation.NonNull;

import java.util.List;

import com.example.sampleapp.data.database.CommentDao;
import com.example.sampleapp.data.entity.CommentEntity;
import io.reactivex.Completable;
import io.reactivex.Flowable;

public class LocalCommentDataStore implements CommentDataStore {

    @NonNull
    private final CommentDao commentDao;

    public LocalCommentDataStore(@NonNull CommentDao commentDao) {
        this.commentDao = commentDao;
    }

    @Override
    public Flowable<List<CommentEntity>> getCommentByPostId(int postId) {
        return commentDao.getCommentByPostId(postId);
    }

    @Override
    public Flowable<List<CommentEntity>> getComments() {
        return commentDao.getAll();
    }

    @Override
    public Completable saveComments(List<CommentEntity> commentList) {
        return Completable.create(emitter -> {
            commentDao.insertAll(commentList);
            emitter.onComplete();
        });
    }
}
