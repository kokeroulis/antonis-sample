package com.example.sampleapp.data.database;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;
import android.support.annotation.NonNull;

import com.example.sampleapp.data.entity.CommentEntity;
import com.example.sampleapp.data.entity.PostEntity;
import com.example.sampleapp.data.entity.UserEntity;

@Database(entities = { PostEntity.class, UserEntity.class, CommentEntity.class},
    version = 1, exportSchema = false)
public abstract class ApplicationDatabase extends RoomDatabase {

    @NonNull
    public abstract PostDao postDao();

    @NonNull
    public abstract UserDao userDao();

    @NonNull
    public abstract CommentDao commentDao();
}
