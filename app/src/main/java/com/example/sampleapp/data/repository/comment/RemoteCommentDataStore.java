package com.example.sampleapp.data.repository.comment;

import android.support.annotation.NonNull;

import java.util.List;

import com.example.sampleapp.data.entity.CommentEntity;
import com.example.sampleapp.data.entity.mapper.CommentResponseMapper;
import com.example.sampleapp.data.network.RestService;
import io.reactivex.Completable;
import io.reactivex.Flowable;
import io.reactivex.Observable;
import io.reactivex.schedulers.Schedulers;

public class RemoteCommentDataStore implements CommentDataStore {
    @NonNull
    private final RestService restService;
    @NonNull
    private final CommentResponseMapper commentResponseMapper;

    public RemoteCommentDataStore(@NonNull RestService restService, @NonNull CommentResponseMapper commentResponseMapper) {
        this.restService = restService;
        this.commentResponseMapper = commentResponseMapper;
    }

    @Override
    public Flowable<List<CommentEntity>> getCommentByPostId(int postId) {
        throw new IllegalArgumentException("fetching remote comments by postId is not supported");
    }

    @Override
    public Flowable<List<CommentEntity>> getComments() {
        return restService.getCommentList()
            .subscribeOn(Schedulers.io())
            .flatMapObservable(Observable::fromIterable)
            .map(commentResponseMapper::transform)
            .toList()
            .toFlowable();
    }

    @Override
    public Completable saveComments(List<CommentEntity> commentList) {
        throw new IllegalArgumentException("Post request are not supported");
    }
}
