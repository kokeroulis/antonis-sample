package com.example.sampleapp.data.repository.comment;

import java.util.List;

import com.example.sampleapp.data.entity.CommentEntity;
import io.reactivex.Completable;
import io.reactivex.Flowable;

public interface CommentDataStore {

    Flowable<List<CommentEntity>> getCommentByPostId(int postId);

    Flowable<List<CommentEntity>> getComments();

    Completable saveComments(List<CommentEntity> commentList);
}
