package com.example.sampleapp.data.repository.user;

import javax.inject.Named;
import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import com.example.sampleapp.data.database.ApplicationDatabase;
import com.example.sampleapp.data.entity.mapper.UserMapper;
import com.example.sampleapp.data.entity.mapper.UserResponseMapper;
import com.example.sampleapp.data.network.RestService;
import com.example.sampleapp.domain.UserRepository;

@Module
public class UserDataModule {
    private static final String LOCAL_STORE = "local_store";
    private static final String REMOTE_STORE = "remote_store";

    @Provides
    @Singleton
    @Named(LOCAL_STORE)
    UserDataStore providesLocalDataStore(ApplicationDatabase applicationDatabase) {
        return new LocalUserDataStore(applicationDatabase.userDao());
    }

    @Provides
    @Singleton
    @Named(REMOTE_STORE)
    UserDataStore providesRemoteDataStore(RestService restService, UserResponseMapper responseMapper) {
        return new RemoteUserDataStore(restService, responseMapper);
    }

    @Provides
    @Singleton
    UserRepository providesPostRepository(@Named(LOCAL_STORE) UserDataStore localDataStore,
                                          @Named(REMOTE_STORE) UserDataStore remoteDataStore,
                                          UserMapper userMapper) {
        return new UserDataRepository(localDataStore, remoteDataStore, userMapper);
    }
}
