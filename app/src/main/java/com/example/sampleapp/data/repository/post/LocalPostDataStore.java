package com.example.sampleapp.data.repository.post;

import android.support.annotation.NonNull;

import java.util.List;

import com.example.sampleapp.data.database.PostDao;
import com.example.sampleapp.data.entity.PostEntity;
import io.reactivex.Completable;
import io.reactivex.Flowable;

public class LocalPostDataStore implements PostDataStore {
    @NonNull
    private final PostDao postDao;

    public LocalPostDataStore(@NonNull PostDao postDao) {
        this.postDao = postDao;
    }

    @Override
    public Flowable<List<PostEntity>> getPosts() {
        return postDao.getAll();
    }


    @Override
    public Flowable<List<PostEntity>> getPostById(int postId) {
        return postDao.getPostById(postId);
    }

    @Override
    public Completable savePosts(List<PostEntity> postList) {
        return Completable.create(emitter -> {
            postDao.insertAll(postList);
            emitter.onComplete();
        });
    }
}
