package com.example.sampleapp.data.network;

import android.support.annotation.NonNull;

import java.util.concurrent.atomic.AtomicReference;

public class ReplaceBaseUrl {
    public static final String PRODUCTION_API_URL = "http://jsonplaceholder.typicode.com/";
    @NonNull
    private final AtomicReference<String> baseUrl;

    public ReplaceBaseUrl(@NonNull String baseUrl) {
        this.baseUrl = new AtomicReference<>(baseUrl);
    }

    public ReplaceBaseUrl() {
        this(PRODUCTION_API_URL);
    }

    public AtomicReference<String> getBaseUrl() {
        return baseUrl;
    }

    public void changeUrl(@NonNull String url) {
        baseUrl.set(url);
    }
}
