package com.example.sampleapp.data.entity.mapper;

import android.support.annotation.NonNull;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.inject.Inject;

import com.example.sampleapp.data.entity.UserEntity;
import com.example.sampleapp.domain.models.User;

public class UserMapper {

    @Inject
    public UserMapper() {}

    @NonNull
    public List<User> transform(@NonNull List<UserEntity> userEntityList) {
        final List<User> userList = new ArrayList<>();

        for (UserEntity entity : userEntityList) {
            final User user = transform(entity);
            userList.add(user);
        }

        return Collections.unmodifiableList(userList);
    }

    @NonNull
    public User transform(@NonNull UserEntity userEntity) {
        return User.builder()
            .id(userEntity.getId())
            .userName(userEntity.getUserName())
            .email(userEntity.getEmail())
            .build();
    }
}
