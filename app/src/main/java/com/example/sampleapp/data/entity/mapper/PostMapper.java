package com.example.sampleapp.data.entity.mapper;

import android.support.annotation.NonNull;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.inject.Inject;

import com.example.sampleapp.data.entity.PostEntity;
import com.example.sampleapp.domain.models.Post;

public class PostMapper {

    @Inject
    public PostMapper() {}

    @NonNull
    public List<Post> transformToPost(@NonNull List<PostEntity> postEntities) {
        final List<Post> postList = new ArrayList<>();
        for (PostEntity postEntity : postEntities) {
            final Post post = transform(postEntity);
            postList.add(post);
        }

        return Collections.unmodifiableList(postList);
    }

    @NonNull
    public Post transform(@NonNull PostEntity postEntity) {
        return Post.builder()
            .id(postEntity.getId())
            .userId(postEntity.getUserId())
            .title(postEntity.getTitle())
            .body(postEntity.getBody())
            .build();
    }

    @NonNull
    public List<PostEntity> transformToEntity(@NonNull List<Post> postList) {
        final List<PostEntity> postEntityList = new ArrayList<>();
        for (Post post : postList) {
            final PostEntity entity = new PostEntity();
            entity.setId(post.id());
            entity.setUserId(post.userId());
            entity.setTitle(post.title());
            entity.setBody(post.body());
            postEntityList.add(entity);
        }

        return Collections.unmodifiableList(postEntityList);
    }
}
