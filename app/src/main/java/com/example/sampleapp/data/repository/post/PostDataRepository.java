package com.example.sampleapp.data.repository.post;

import android.support.annotation.NonNull;

import java.util.List;

import com.example.sampleapp.data.entity.mapper.PostMapper;
import com.example.sampleapp.domain.PostRepository;
import com.example.sampleapp.domain.models.Post;
import io.reactivex.Completable;
import io.reactivex.Flowable;
import io.reactivex.schedulers.Schedulers;

public class PostDataRepository implements PostRepository {

    @NonNull
    private final PostDataStore postLocalDataStore;
    @NonNull
    private final PostDataStore postRemoteDataStore;
    @NonNull
    private final PostMapper postMapper;

    public PostDataRepository(@NonNull PostDataStore postLocalDataStore, @NonNull PostDataStore postRemoteDataStore,
                              @NonNull PostMapper postMapper) {
        this.postLocalDataStore = postLocalDataStore;
        this.postRemoteDataStore = postRemoteDataStore;
        this.postMapper = postMapper;
    }

    @Override
    @NonNull
    public Flowable<List<Post>> getPosts() {
        return postLocalDataStore.getPosts()
            .subscribeOn(Schedulers.io())
            .map(postMapper::transformToPost);
    }

    @NonNull
    @Override
    public Flowable<Post> findPostById(int postId) {
        return postLocalDataStore.getPostById(postId)
            .filter(postList -> postList.size() > 0)
            .map(postList -> postList.get(0))
            .map(postMapper::transform);
    }

    @Override
    @NonNull
    public Completable forceUpdate() {
        return postRemoteDataStore.getPosts()
            .subscribeOn(Schedulers.io())
            .firstOrError()
            .flatMapCompletable(postLocalDataStore::savePosts);
    }
}
