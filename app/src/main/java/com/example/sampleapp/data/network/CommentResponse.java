package com.example.sampleapp.data.network;

import android.support.annotation.NonNull;

import com.google.auto.value.AutoValue;
import com.google.gson.Gson;
import com.google.gson.TypeAdapter;

@AutoValue
public abstract class CommentResponse {
    public abstract int id();
    public abstract int postId();
    public abstract String name();
    public abstract String email();
    public abstract String body();

    @NonNull
    public static CommentResponse.Builder builder() {
        return new AutoValue_CommentResponse.Builder();
    }

    @NonNull
    public static TypeAdapter<CommentResponse> typeAdapter(@NonNull final Gson gson) {
        return new AutoValue_CommentResponse.GsonTypeAdapter(gson);
    }

    @AutoValue.Builder
    public interface Builder {
        Builder id(int id);
        Builder postId(int postId);
        Builder name(String name);
        Builder email(String email);
        Builder body(String body);
        CommentResponse build();
    }
}
