package com.example.sampleapp.data.repository.post;

import android.support.annotation.NonNull;

import java.util.List;

import com.example.sampleapp.data.entity.PostEntity;
import com.example.sampleapp.data.network.RestService;
import com.example.sampleapp.data.entity.mapper.PostResponseMapper;
import io.reactivex.Completable;
import io.reactivex.Flowable;
import io.reactivex.Observable;
import io.reactivex.schedulers.Schedulers;

public class RemoteDataStore implements PostDataStore {

    @NonNull private final RestService restService;
    @NonNull private final PostResponseMapper postResponseMapper;

    public RemoteDataStore(@NonNull RestService restService, @NonNull PostResponseMapper postResponseMapper) {
        this.restService = restService;
        this.postResponseMapper = postResponseMapper;
    }

    @Override
    public Flowable<List<PostEntity>> getPosts() {
        return restService.getPostList()
            .subscribeOn(Schedulers.io())
            .flatMapObservable(Observable::fromIterable)
            .map(postResponseMapper::transform)
            .toList()
            .toFlowable();
    }

    @Override
    public Flowable<List<PostEntity>> getPostById(int postId) {
        throw new IllegalArgumentException("Get request by id is not supported");
    }

    @Override
    public Completable savePosts(List<PostEntity> postList) {
        throw new IllegalArgumentException("Post request are not supported");
    }
}
