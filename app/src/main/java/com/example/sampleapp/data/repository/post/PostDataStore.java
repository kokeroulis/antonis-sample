package com.example.sampleapp.data.repository.post;

import java.util.List;

import com.example.sampleapp.data.entity.PostEntity;
import io.reactivex.Completable;
import io.reactivex.Flowable;

public interface PostDataStore {

    Flowable<List<PostEntity>> getPosts();

    Flowable<List<PostEntity>> getPostById(int postId);

    Completable savePosts(List<PostEntity> postList);
}
