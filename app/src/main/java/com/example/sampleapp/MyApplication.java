package com.example.sampleapp;

import android.app.Activity;
import android.app.Application;

import com.facebook.stetho.Stetho;
import com.squareup.leakcanary.LeakCanary;

import javax.inject.Inject;

import dagger.android.AndroidInjector;
import dagger.android.DispatchingAndroidInjector;
import dagger.android.HasActivityInjector;
import com.example.sampleapp.dagger.components.ApplicationComponent;
import com.example.sampleapp.dagger.components.DaggerApplicationComponent;
import com.example.sampleapp.dagger.modules.ApplicationModule;
import timber.log.Timber;
import timber.log.Timber.DebugTree;

public class MyApplication extends Application implements HasActivityInjector {
    // required for unit test
    protected static ApplicationComponent applicationComponent;

    @Inject
    DispatchingAndroidInjector<Activity> dispatchingActivityInjector;

    @Override
    public AndroidInjector<Activity> activityInjector() {
        return dispatchingActivityInjector;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        applicationComponent = createComponent();
        applicationComponent.inject(this);

        if (BuildConfig.DEBUG) {
            Timber.plant(new DebugTree());
            Stetho.initializeWithDefaults(this);
        }

        LeakCanary.install(this);
    }

    private ApplicationComponent createComponent() {
        return DaggerApplicationComponent.builder()
            .applicationModule(new ApplicationModule(this))
            .build();
    }
}
