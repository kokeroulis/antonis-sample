# General Structure

This project is being implemented using Java (6/7) and with retrolambda,
and it is using the Android Gradle Plugin 2.3. I am familiar with kotlin and the
Android Gradle Plugin 3.0 but i wanted to keep it as simple as possible.

The project also contains unit tests for the integration with the remote resource,
mappers, repositories, interactors and for the presenters. They can be executed 
from the command line (e.x. ./gradlew :app:testDebugUnitTest)


# Libraries and architectures that are being used

Bellow you will find a list of the most important libraries and architectures
that are being used by this project.

Clean Architecture
AutoValue
RxJava2
Dagger 2 (2.11)
Room database
Glide
Retrofit2 (Http)
MVP (with Mosby 3)
Mockito
Robolectric 

# Details about the implementation

In the following project i am using a separate model for each 
layer(data, domain, presentation) and also for each service.

The db model and the network response model are two different models,
despite the fact that it could be just one.
I have followed this approach in order to demonstrate how we should handle
cases in which the entity model requires more than one network requests in order
to be constructed.

The implemented repositories contain a method called "forceUpdate" which
can be called from the interactor in order to update the data. I followed
this approach in order to be easier to integrate refresh mechanisms like 
pull to refresh.

The presenters can survive the configuration changes because Mosby v3
can handle this for us. If we had to user another MVP library then we
could store our presenters inside a headless fragment in order to survive
configuration changes.